#!/bin/sh -e

VERSION=$2
TAR=../jspwiki_$VERSION.orig.tar.gz
DIR=jspwiki-$VERSION
TAG=$(echo jspwiki_$VERSION | sed -e 's/\./_/g')

svn export http://svn.apache.org/repos/asf/incubator/jspwiki/tags/$TAG $DIR
tar -c -z -f $TAR --exclude '*.jar' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi
