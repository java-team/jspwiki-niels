/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.cert;

import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Lightweight wrapper object for an X.509 certificate distinguished name.
 * 
 * @author Andrew R. Jaquith
 * @version $Revision: 1.1 $ $Date: 2006/04/19 23:34:44 $
 */
public final class CertificateDN implements Principal {

  private String commonName = null;

  private String country = null;

  private String domainComponent = null;

  private String email = null;

  private String locality = null;

  private String name;

  private String organization = null;

  private String organizationalUnit = null;

  private String state = null;

  /**
   * Constructs a new CertificateDN by parsing a supplied Principal from a
   * certificate, such as {@link X509Certificate#getSubjectDN()} or
   * {@link X509Certificate#getIssuerDN()}.
   * 
   * @param dn a Principal representing a certificate distinguished name
   * 
   */
  public CertificateDN(Principal dn) {
    if (dn == null) {
      throw new IllegalArgumentException("Disinguished name cannot be null.");
    }
    this.name = dn.getName();

    Pattern p = Pattern.compile("(CN|E|OU|O|L|ST|C|DC)=(\"{0,1})(.+?)(\\2),");
    Matcher m = p.matcher(dn.getName() + ",");
    while (m.find()) {
      String key = m.group(1);
      String value = m.group(3);
      if ("CN".equals(key)) {
        commonName = value;
      }
      else if ("E".equals(key)) {
        email = value;
      }
      else if ("OU".equals(key)) {
        organizationalUnit = value;
      }
      else if ("O".equals(key)) {
        organization = value;
      }
      else if ("L".equals(key)) {
        locality = value;
      }
      else if ("ST".equals(key)) {
        state = value;
      }
      else if ("C".equals(key)) {
        country = value;
      }
      else if ("DC".equals(key)) {
        domainComponent = value;
      }
    }
  }

  /**
   * Returns the common name
   * 
   * @return the common name
   */
  public final String getCommonName() {
    return commonName;
  }

  /**
   * Returns the country.
   * 
   * @return the country
   */
  public final String getCountry() {
    return country;
  }

  /**
   * Returns the domain component.
   * 
   * @return the domain component
   */
  public final String getDomainComponent() {
    return domainComponent;
  }

  /**
   * Returns the email.
   * 
   * @return the email
   */
  public final String getEmail() {
    return email;
  }

  /**
   * Returns the locality.
   * 
   * @return the locality
   */
  public final String getLocality() {
    return locality;
  }

  /**
   * Returns the distinguished name.
   * 
   * @return the distinguished name.
   */
  public final String getName() {
    return name;
  }

  /**
   * Returns the organization.
   * 
   * @return the organization
   */
  public final String getOrganization() {
    return organization;
  }

  /**
   * Returns the organizational unit.
   * 
   * @return the organizational unit
   */
  public final String getOrganizationalUnit() {
    return organizationalUnit;
  }

  /**
   * Returns the state.
   * 
   * @return the state
   */
  public final String getState() {
    return state;
  }

}
