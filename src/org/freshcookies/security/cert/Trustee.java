/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.cert;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * <p>Utility class that performs common certificate trust operations, such as
 * writing to disk, updating the trust database and printing verbose certficate
 * info.</p>
 * <p>This class requires privileges to run with a SecurityManager.
 * At a mimumum, this ProtectionDomain and preceding callers must be granted the following
 * permissions:</p>
 * <ul>
 *   <li><code>java.util.PropertyPermission "javax.net.ssl.trustStore", "read"</code></li>
 *   <li><code>java.util.PropertyPermission "java.home", "read"</code></li>
 *    <li><code>java.io.FilePermission "${java.home}/lib/security/cacerts", "read"</code></li>
 * </ul>
 * <p>In addition, if the method {@link #saveCertificate(X509Certificate)}
 * is called, this ProtectionDomain and all preceding callers must also be 
 * granted these additional permissions:</p>
 * <ul>
 *   <li><code>java.lang.RuntimePermission "writeFileDescriptor"</code></li>
 *   <li><code>java.io.FilePermission "${user.dir}/*.cer", "write"</code></li>
 * </ul>
 * <p>The {@link #commit()} method requires that this ProtectionDomain and
 * all preceding callers be granted these additional permisions:</p>
 * <ul>
 *   <li><code>java.lang.RuntimePermission "writeFileDescriptor"</code></li>
 *   <li><code>java.io.FilePermission "${java.home}/lib/security/cacerts", "write"</code></li>
 * </ul>
 * <p>None of the methods in this class perform their actions inside 
 * <code>doPrivileged</code> blocks, so all {@link SecurityException} errors
 * are propagated to callers.</code></p>
 * @author Andrew R. Jaquith
 * @version $Revision: 1.3 $ $Date: 2007/02/24 17:27:28 $
 */
public class Trustee {

  private boolean certsAdded;

  private final X509TrustManager sslTrustManager;

  private String trustedCAPath;

  private final KeyStore trustedCAStore;

  /**
   * Constructs a new Trustee instance, and initializes the system certificate
   * authority (CA) keystore and SSL trust manager.
   * @throws SecurityException when the SecurityManager is running, and the current 
   * policy does not grant this ProtectionDomain and preceding callers all of the following
   * permissions:
   * <ul><li><code>java.util.PropertyPermission "javax.net.ssl.trustStore", "read"</code></li>
   * <li><code>java.util.PropertyPermission "java.home" "read"</code></li>
   * <li><code>java.io.FilePermission "${java.home}/lib/security/cacerts", "read"</code></li></ul>
   */
  public Trustee() {
    super();
    trustedCAStore = initSystemCAStore();
    sslTrustManager = initSSLTrustManager();
    certsAdded = false;
  }

  /**
   * Calculates an alias for a certificate by trying the common name,
   * organizational unit, DC and serial number in succession.
   * 
   * @param cert the certificate
   * @return the alias
   */
  public static String getAlias(X509Certificate cert) {
    CertificateDN dn = new CertificateDN(cert.getSubjectDN());
    if (dn.getCommonName() != null) {
      return dn.getCommonName();
    }
    if (dn.getOrganizationalUnit() != null) {
      return dn.getOrganizationalUnit() + "-"
          + cert.getSerialNumber().toString();
    }
    if (dn.getDomainComponent() != null) {
      return dn.getDomainComponent() + "-" + cert.getSerialNumber().toString();
    }
    return cert.getSerialNumber().toString();
  }

  /**
   * Returns a string containing verbose certificate information. The format is
   * identical to that produced by the J2SE <code>keytool</code> program.
   * 
   * @param cert the certificate to examine
   */
  public static String getCertificateInfo(X509Certificate cert) {
    String date = DateFormat.getDateInstance(DateFormat.MEDIUM).format(
        cert.getNotBefore());
    String info = "Creation date: " + date + "\n";
    info += "Owner:\n";
    CertificateDN s = new CertificateDN(cert.getSubjectDN());
    info += (s.getCommonName() != null) ? "         CN=" + s.getCommonName()
        + "\n" : "";
    info += (s.getOrganization() != null) ? "         O=" + s.getOrganization()
        + "\n" : "";
    info += (s.getOrganizationalUnit() != null) ? "         OU="
        + s.getOrganizationalUnit() + "\n" : "";
    info += (s.getDomainComponent() != null) ? "         DC="
        + s.getDomainComponent() + "\n" : "";
    info += (s.getLocality() != null) ? "         L=" + s.getLocality() + "\n"
        : "";
    info += (s.getState() != null) ? "         S=" + s.getState() + "\n" : "";
    info += (s.getCountry() != null) ? "         C=" + s.getCountry() + "\n"
        : "";
    info += (s.getEmail() != null) ? "         E=" + s.getEmail() + "\n" : "";
    info += "Issuer:\n";
    s = new CertificateDN(cert.getIssuerDN());
    info += (s.getCommonName() != null) ? "         CN=" + s.getCommonName()
        + "\n" : "";
    info += (s.getOrganization() != null) ? "         O=" + s.getOrganization()
        + "\n" : "";
    info += (s.getOrganizationalUnit() != null) ? "         OU="
        + s.getOrganizationalUnit() + "\n" : "";
    info += (s.getDomainComponent() != null) ? "         DC="
        + s.getDomainComponent() + "\n" : "";
    info += (s.getLocality() != null) ? "         L=" + s.getLocality() + "\n"
        : "";
    info += (s.getState() != null) ? "         S=" + s.getState() + "\n" : "";
    info += (s.getCountry() != null) ? "         C=" + s.getCountry() + "\n"
        : "";
    info += (s.getEmail() != null) ? "         E=" + s.getEmail() + "\n" : "";
    info += "Serial number: " + cert.getSerialNumber() + "\n";
    info += "Valid from: " + cert.getNotBefore() + " until: "
        + cert.getNotAfter() + "\n";
    info += "Certificate fingerprints:\n";
    info += "         MD5:  " + getCertFingerPrint("MD5", cert) + "\n";
    info += "         SHA1: " + getCertFingerPrint("SHA1", cert) + "\n";
    return info;
  }

  /**
   * Appends an ASCII representation of a byte to a supplied StringBuffer.
   * 
   * @param byte0 the byte to encode
   * @param stringbuffer the buffer to append to
   */
  private static void byte2hex(byte byte0, StringBuffer stringbuffer) {
    char ac[] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
        'E', 'F'
    };
    int i = (byte0 & 0xf0) >> 4;
    int j = byte0 & 0xf;
    stringbuffer.append(ac[i]);
    stringbuffer.append(ac[j]);
  }

  /**
   * Calculates the fingerprint for a certificate and returns it as a string.
   * 
   * @param s the digest algorithm to use
   * @param certificate the certificate
   * @return the fingerprint
   */
  private static String getCertFingerPrint(String s, Certificate certificate) {
    try {
      byte abyte0[] = certificate.getEncoded();
      MessageDigest messagedigest = MessageDigest.getInstance(s);
      byte abyte1[] = messagedigest.digest(abyte0);
      return toHexString(abyte1);
    }
    catch (Exception e) {
      return "(error)";
    }
  }

  /**
   * Encodes an array of bytes and its ASCII string representation.
   * 
   * @param abyte0 the array of bytes to encode
   * @return the string
   */
  private static String toHexString(byte abyte0[]) {
    StringBuffer stringbuffer = new StringBuffer();
    int i = abyte0.length;
    for (int j = 0; j < i; j++) {
      byte2hex(abyte0[j], stringbuffer);
      if (j < i - 1)
        stringbuffer.append(":");
    }

    return stringbuffer.toString();
  }

  /**
   * <p>
   * Adds a supplied CA certificate to the system certificate trust store as a
   * "trusted CA certificate". This is equivalent to the following command-line
   * action:
   * </p>
   * <code>keytool -import -trustcacerts -keystore <var>$JAVA_HOME</var>/lib/security/cacerts -file <var>certificate</var></code>
   * <p>
   * Note: a useful argument for debugging is <code>-Djavax.net.debug=all</code>.
   * </p>
   * 
   * @param cert the certificate to add to the trust store
   * @return <code>true</code> if successfully added; <code>false</code> if
   *         not
   * @throws KeyStoreException if the CA alias ould not be added to the trust store
   */
  public boolean trustCACertificate(X509Certificate cert)
      throws KeyStoreException {
    if (!isTrustedCA(cert)) {
      System.out.println("This is a CA certificate. It is NOT trusted.");
      try {
        if (yesResponse("Do you want to trust this certificate?")) {
          String alias = Trustee.getAlias(cert);
          System.out.println("Adding CA to trust store with alias " + alias);
          trustedCAStore.setCertificateEntry(alias, cert);
          System.out.println("..success");
          certsAdded = true;
          return true;
        }
      }
      catch (IOException e) {
        System.out.println("Could not add certificate: "
            + e.getLocalizedMessage());
        return false;
      }
    }
    System.out.println("This is a CA certificate. It is already trusted.");
    return false;
  }

  /**
   * Writes the trust store to disk. The caller must have write access to the 
   * systemwide trust store.
   * @return <code>true</code> if the commit succeeds; <code>false</code> if
   *         not
   * @throws SecurityException when the SecurityManager is running, and the current policy does
   * not grant this ProtectionDomain and preceding callers all of the following
   * permissions: <ul>
   * <li><code>java.lang.RuntimePermission "writeFileDescriptor"</code></li>
   * <li><code>java.io.FilePermission "${java.home}/lib/security/cacerts", "write"</code></li></ul>
   */
  public boolean commit() {
    if (!certsAdded) {
      System.err.println("No need to commit (no certificates added).");
      return false;
    }
    try {
      File file = new File(trustedCAPath);
      if (file.canWrite()) {
        OutputStream out = new FileOutputStream(trustedCAPath);
        trustedCAStore.store(out, "changeit".toCharArray());
        out.close();
        return true;
      }
      System.out.println("FATAL: You do not have write privileges "
          + "to the Java JSSE trust store " + trustedCAPath
          + "\n\nTry running the application using sudo, or as root.\n");
    }
    catch (CertificateException e) {
      System.out.println("Certificate exception: " + e.getMessage());
    }
    catch (NoSuchAlgorithmException e) {
      System.out.println("No such algorithm: " + e.getMessage());
    }
    catch (KeyStoreException e) {
      System.out.println("Keystore exception: " + e.getMessage());
    }
    catch (IOException e) {
      System.out.println("IO exception: " + e.getMessage());
    }
    return false;
  }

  /**
   * Saves an X.509 certificate as a binary file in the current directory. The
   * name of the file is calculated by taking the certificate's common name and
   * appending the suffix <code>.cer</code>. This suffix is sufficiently
   * cross-platform that Mac OS X and Windows users alike can simply
   * double-click on the file to install it using the respective certificate
   * management tool.
   * 
   * @param cert the certificate to save
   * @throws IOException if the file could not be written
   * @throws CertificateEncodingException if the certificate could not be properly encoded
   * @throws SecurityException when the SecurityManager is running, and the current policy does
   * not grant this ProtectionDomain and preceding callers all of the following
   * permissions: <ul>
   * <li><code>java.lang.RuntimePermission "writeFileDescriptor"</code></li>
   * <li><code>java.io.FilePermission "${user.dir}/*.cer", "write"</code></li></ul>
   */
  public void saveCertificate(X509Certificate cert) throws IOException,
      CertificateEncodingException {
    String alias = Trustee.getAlias(cert);
    alias = alias.replaceAll("[,\\.\\\\/]", "");
    String file = System.getProperty("user.dir") + "/" + alias + ".cer";
    FileOutputStream out = new FileOutputStream(file);
    out.write(cert.getEncoded());
    out.close();
    System.out.println("Saved certificate as " + file);
  }

  /**
   * Initializes and returns the system JSSE trust manager. The algorithm used
   * is the default returned by the system property
   * <code>ssl.TrustManagerFactory.algorithm</code>, which in normal
   * cicrcumstances should be <code>SunX509</code>. The keystore used for
   * trusted CA certificates is the one previously initialized by
   * {@link #initSystemCAStore()}.
   * @return the SSL trust manager, initialized
   */
  private X509TrustManager initSSLTrustManager() {
    TrustManager[] managers = new TrustManager[] {};
    try {
      TrustManagerFactory factory = TrustManagerFactory
          .getInstance(TrustManagerFactory.getDefaultAlgorithm());
      factory.init(trustedCAStore);
      managers = factory.getTrustManagers();
    }
    catch (KeyStoreException e) {
      System.out
          .println("Could not initialize trust manager with system keystore: "
              + e.getMessage());
    }
    catch (NoSuchAlgorithmException e) {
      System.out.println("No such algorithm: " + e.getMessage());
    }
    for (int i = 0; i < managers.length; i++) {
      if (managers[i] instanceof X509TrustManager) {
        return (X509TrustManager) managers[i];
      }
    }
    return null;
  }

  /**
   * Initializes and returns the KeyStore where JSSE trusted CA certificates are
   * stored. The method first checks to see if the
   * <code>javax.net.ssl.trustStore</code> property is set; if it is, the
   * keystore at that location is loaded. If not, the keystore in the default
   * location <code><var>JAVA_HOME</var>/lib/security/cacerts</code> is
   * loaded.
   * 
   * @return the JSSE trust store, initialized
   * @throws SecurityException when the SecurityManager is running, and the current policy does
   * not grant this ProtectionDomain and preceding callers all of the following
   * permissions: <code>java.util.PropertyPermission "javax.net.ssl.trustStore"</code>,
   * <code>java.util.PropertyPermission "java.home"</code>,
   * <code>java.io.FilePermission "${java.home}/lib/security/cacerts", "read"</code>
   */
  private KeyStore initSystemCAStore() {

    // Ask where the JRE thinks the trust store is
    this.trustedCAPath = System.getProperty("javax.net.ssl.trustStore");
    if (trustedCAPath == null) {
      trustedCAPath = System.getProperty("java.home") + "/lib/security/cacerts";
    }

    // Now, open it
    KeyStore store = null;
    try {
      store = KeyStore.getInstance(KeyStore.getDefaultType());

      InputStream fis = new FileInputStream(trustedCAPath);
      System.out.println("Locating certificate trust store: " + trustedCAPath);
      store.load(fis, "changeit".toCharArray());
      fis.close();
    }
    catch (FileNotFoundException e) {
      System.out.println("Could not open keystore file: " + e.getMessage());
    }
    catch (IOException e) {
      System.out.println("IO exception: " + e.getMessage());
    }
    catch (CertificateException e) {
      System.out.println("Certificate exception: " + e.getMessage());
    }
    catch (KeyStoreException e) {
      System.out.println("Could not get keystore: " + e.getMessage());
    }
    catch (NoSuchAlgorithmException e) {
      System.out.println("No such algorithm: " + e.getMessage());
    }
    return store;
  }

  /**
   * Determines whether a supplied CA certificate is considered "trusted" by a
   * TrustManager. To determine whether the certificate is already trusted, the
   * supplied TrustManager's <code>getAcceptedIssuers</code> method is
   * consulted.
   * 
   * @param cert the X.509 certificate to test
   * @return <code>true</code> if the certificate is trusted,
   *         <code>false</code> if not
   */
  private boolean isTrustedCA(X509Certificate cert) {
    Certificate[] trustedCAs = sslTrustManager.getAcceptedIssuers();
    boolean isCertTrusted = false;
    for (int j = 0; j < trustedCAs.length; j++) {
      if (cert.equals(trustedCAs[j])) {
        isCertTrusted = true;
        break;
      }
    }
    return isCertTrusted;
  }

  /**
   * Obtains a "yes" or "no" response from standard input.
   * 
   * @param prompt the prompt
   * @return <code>true</code> if the response is "yes", <code>false</code>
   *         otherwise
   */
  private synchronized boolean yesResponse(String prompt) throws IOException {
    String response = " ";
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    while (!"YES".equals(response) && !"NO".equals(response)) {
      System.out.print(prompt + " (yes/no): ");
      response = reader.readLine().toUpperCase().trim();
    }
    return ("YES".equals(response));
  }
}
