/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.cert;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * <p>Extracts certificates used to sign a specified 
 * JAR file and optionally save them to disk. If the SecurityManager is running, the 
 * ProtectionDomain of this class must grant read access to any jar files
 * passed to {@link #extractCACertificates(JarFile)} or
 * {@link #extractSigningCertificates(JarFile)}. For example, to process
 * the jar file <code>/etc/myclasses.jar</code>, the security
 * policy file should grant the following permission to the
 * <code>freshcookies-securty-<em>version</em>.jar</code> CodeSource and
 * all preceding callers:</p>
 * <blockquote><code>permission java.io.FilePermision "/etc/myclasses.jar", "read"</code></blockquote>
 * <p>In addition, if the stand-alone {@link #main(String[])} method is used to automatically
 * extract discovered certificates or add them to the system-wide trust store, this 
 * CodeSource and all preceding callsers require these additional privileges:</p>
 * <ul>
 *   <li><code>java.io.FilePermission "${java.home}/lib/security/cacerts", "read,write"</code></li>
 *   <li><code>java.lang.RuntimePermission "writeFileDescriptor"</code></li>
 *   <li><code>java.util.PropertyPermission "javax.net.ssl.trustStore", "read"</code></li>
 *   <li><code>java.util.PropertyPermission "java.home" "read"</code></li>
 * </ul>
 * <p>None of the methods in this class perform their actions inside 
 * <code>doPrivileged</code> blocks, so all {@link SecurityException} errors
 * are propagated to callers.</code></p>
 * @author Andrew R. Jaquith
 * @version $Revision: 1.4 $ $Date: 2007/02/24 17:27:28 $
 */
public class JarHelper {

  private Map caCache = new HashMap();

  private Map certCache = new HashMap();

  private Set jars = new HashSet();
  
  /**
   * Constructs a new instance of JarHelper.
   */
  public JarHelper() {
	  super();
  }

  /**
   * Convenience main method that extracts the signing certificates from a jar file
   * and optionally saves them to disk and the system trust store.
   * @param args the absolute path to the jar file, or <code>--help</code> to print
   * a short help message.
   * @throws SecurityException when the SecurityManager is running, and the current 
   * policy does not grant this ProtectionDomain and preceding callers all of the following
   * permissions:
   * <ul>
   *   <li><code>java.io.FilePermission "${java.home}/lib/security/cacerts", "read,write"</code></li>
   *   <li><code>java.lang.RuntimePermission "writeFileDescriptor"</code></li>
   *   <li><code>java.util.PropertyPermission "javax.net.ssl.trustStore", "read"</code></li>
   *   <li><code>java.util.PropertyPermission "java.home" "read"</code></li>
   * </ul>
   */
  public static void main(String[] args) {
    if (args.length == 0) {
      System.err.println("FATAL: you must supply a jar file (e.g., foo.jar)");
      System.exit(1);
    }

    // Display help if the user asked for it
    if (args[0].equals("--help") | args[0].equals("-h")) {
      System.out.println("Usage: JarHelper jarfile");
      System.exit(0);
    }

    // Set the jar file
    String file = args[0];

    Trustee trustee = new Trustee();
    JarHelper helper = new JarHelper();

    // Open the jar and get its signing certificates
    System.out.print("Extracting signing certificates from " + file + "... ");
    Set cas = new HashSet();
    Set signers = new HashSet();
    try {
      JarFile jar = new JarFile(new File(file), true);
      cas = helper.extractCACertificates(jar);
      signers = helper.extractSigningCertificates(jar);
    }
    catch (IOException e) {
      System.err.println("Couldn't get jar certificates: "
          + e.getLocalizedMessage());
    }

    // Offer to add untrusted CA certificates to trust store
    System.out.println("Found " + cas.size() + " CA certificates.");
    boolean certsAdded = false;
    int i = 0;
    for (Iterator it = cas.iterator(); it.hasNext();) {
      X509Certificate cert = (X509Certificate)it.next();
      System.out.println("CA certificate [" + i + "]:");
      System.out.println(Trustee.getCertificateInfo(cert));
      try {
        certsAdded = certsAdded | trustee.trustCACertificate(cert);
      }
      catch (Exception e) {
        System.err.println("Could not add certificate to trust store: "
            + e.getLocalizedMessage());
      }
      i++;
    }

    // Tell the user about the signing certificates we found
    System.out.println("Found " + signers.size() + " signing certificates.");
    i = 0;
    for (Iterator it = signers.iterator(); it.hasNext();) {
      X509Certificate cert = (X509Certificate)it.next();
      System.out.println(Trustee.getCertificateInfo(cert));
    }

    // Now, save the CA certs and signing certs to disk
    try {
      for (Iterator it = cas.iterator(); it.hasNext();) {
        X509Certificate cert = (X509Certificate)it.next();
        trustee.saveCertificate(cert);
      }
      for (Iterator it = signers.iterator(); it.hasNext();) {
        X509Certificate cert = (X509Certificate)it.next();
        trustee.saveCertificate(cert);
      }
    }
    catch (Exception e) {
      System.err.println("Could not save certificate: "
          + e.getLocalizedMessage());
    }

    // Commit the trust store changes
    System.out.print("Committing changes to trust store... ");
    if (trustee.commit()) {
      System.err.println("done.");
    }
    else {
      System.err.println("trust store not saved.");
    }

  }

  /**
   * Extracts the CA certificates from a Jar file.
   * 
   * @param jar the jar file
   * @return an array of CA {@link java.security.cert.X509Certificate} objects
   * @throws IOException if the jar cannot be read
   * @throws SecurityException when the SecurityManager is running,
   * and it denies read access to the jar file
   */
  public Set extractCACertificates(JarFile jar)
      throws IOException {
    if (!jars.contains(jar)) {
      extractJarCerts(jar);
      jars.add(jar);
    }
    return (Set)caCache.get(jar);
  }

  /**
   * Extracts the signing certificates from a Jar file.
   * 
   * @param jar the jar file
   * @return an array of {@link java.security.cert.X509Certificate} objects used to sign various files
   * @throws IOException if the jar cannot be read
   * @throws SecurityException when the SecurityManager is running,
   * and it denies read access to the jar file
   */
  public Set extractSigningCertificates(JarFile jar)
      throws IOException {
    if (!jars.contains(jar)) {
      extractJarCerts(jar);
      jars.add(jar);
    }
    return (Set)certCache.get(jar);
  }

  /**
   * Extracts signing certificates from a jar file.
   * @param jar
   * @throws IOException if the jar cannot be read
   * @throws SecurityException when the SecurityManager (if running) denies access to the file
   */
  private void extractJarCerts(JarFile jar) throws IOException {
    Set cas = new HashSet();
    Set certs = new HashSet();
    Enumeration entries = jar.entries();
    while (entries.hasMoreElements()) {
      JarEntry entry = (JarEntry) entries.nextElement();
      InputStream is = jar.getInputStream(entry);
      byte[] buf = new byte[1024];
      while (is.read(buf) > 0) {
      }
      is.close();
      Certificate[] signingCerts = entry.getCertificates();
      if (signingCerts != null) {
        for (int i = 0; i < signingCerts.length; i++) {
          Certificate cert = signingCerts[i];
          if (cert instanceof X509Certificate) {
            X509Certificate x509cert = (X509Certificate) cert;
            if (i == 0 && !certs.contains(x509cert)) {
              certs.add(x509cert);
            }
            else if (i > 0 && !cas.contains(x509cert)) {
              cas.add(x509cert);
            }
          }
        }
        
        // If the chain had only one cert, it's self-signed
        if (signingCerts.length == 1) {
          if (signingCerts[0] instanceof X509Certificate) {
            cas.add(signingCerts[0]);
          }
        }
      }
    }
    caCache.put(jar, cas);
    certCache.put(jar, certs);
  }}
