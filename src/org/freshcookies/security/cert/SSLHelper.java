/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.cert;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.net.SocketFactory;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Extracts SSL certificates from a specified webserver and optionally adds them
 * to the JSSE truststore.
 * 
 * @author Andrew R. Jaquith
 * @version $Revision: 1.3 $ $Date: 2007/02/24 17:27:28 $
 */
public class SSLHelper {

  /**
   * Default SSL port.
   */
  protected static final int SSL_PORT = 443;

  /**
   * Helper method that connects to a specified host using SSL and extracts the
   * server's peer certificates. To do this, a new <code>SSLContext</code> is
   * created using a "null" trust manager that accepts all peer SSL certificates
   * as trusted. The null trust manager is provided by the method
   * <code>nullTrustManager</code>. Note that in the array of Certificates
   * returned by this method, the first certficate is the server's own
   * certificate; the ones that follow are the certificate authorities in the
   * chain.
   * 
   * @param hostname
   * @return the remote host's chain of SSL certificates
   */
  public static Certificate[] extractSSLCertificates(String hostname, int port) {
    Certificate certs[] = new Certificate[] {};
    TrustManager trustManager = nullTrustManager();
    SSLSocket socket = createSSLSocket(new TrustManager[] {
      trustManager
    }, hostname, port);
    if (socket != null) {
      try {
        certs = socket.getSession().getPeerCertificates();
        socket.close();
      }
      catch (SSLPeerUnverifiedException e) {
        System.out.println("Could not verify peer: " + e.getMessage());
      }
      catch (IOException e) {
        System.out.println("Could not close socket: " + e.getMessage());
      }
    }
    else {
      System.err.println("could not create SSL socket.");
    }
    return certs;
  }

  public static void main(String[] args) {
    if (args.length == 0) {
      System.err
          .println("FATAL: you must supply a host name (e.g., internal.atstake.com)");
      System.exit(1);
    }

    // Display help if the user asked for it
    if (args[0].equals("--help") | args[0].equals("-h")) {
      System.out.println("Usage: SSLHelper hostname [port]");
      System.exit(0);
    }

    // Set the host name and port (defaults to SSL_PORT)
    String host = args[0];
    int port = SSL_PORT;
    if (args.length > 1) {
      port = Integer.parseInt(args[1]);
    }

    // Connect to the host and get its SSL certificates
    System.out.print("Extracting SSL certificates from " + host + ":" + port
        + "... ");
    Certificate[] certs = extractSSLCertificates(host, port);
    if (certs.length == 0) {
      System.err
          .println("ERROR: No certificates found. Is there an SSL server running on this host?\n");
      System.exit(1);
    }
    System.out.println(certs.length + " certificate"
        + (certs.length == 1 ? "" : "s") + " found.");

    // Loop through each certificate in the chain
    boolean certsAdded = false;

    Trustee trustee = new Trustee();
    for (int i = 0; i < certs.length; i++) {
      if (certs[i] instanceof X509Certificate) {

        // Write each cert to disk as a DER-encoded file
        X509Certificate cert = (X509Certificate) certs[i];
        System.out.println("Certificate[" + i + "]:");
        System.out.println(Trustee.getCertificateInfo(cert));
        try {
          trustee.saveCertificate(cert);
        }
        catch (Exception e) {
          System.err.println("ERROR: could not save certificate. "
              + e.getMessage());
        }

        // Test each CA certificate to see if it is trusted; offer to
        // add it if not
        if (i == 0) {
          System.out.println("This is the server certificate.");
        }
        else {
          try {
            certsAdded = certsAdded | trustee.trustCACertificate(cert);
          }
          catch (KeyStoreException e) {
            System.out.println(e.getLocalizedMessage());
          }
        }
        System.out.println("");
      }
    }

    if (certs.length == 1) {
      System.err
          .println("WARNING: certificate chain did not include issuing CA certificate. How rude!\n");
    }

    // If changes were made to the trust store, write it to disk
    if (certsAdded) {
      trustee.commit();
    }
  }

  /**
   * Constructs a new SSLHelper.
   */
  public SSLHelper() {
    super();
  }

  /**
   * Creates a new SSL socket to a specificed host using the supplied
   * <code>TrustManager</code>. If a connection cannot be made, the method
   * returns null.
   * 
   * @param trustManager
   * @param hostname
   * @return
   */
  private static SSLSocket createSSLSocket(TrustManager[] trustManager,
      String hostname, int port) {
    SSLSocket socket = null;
    try {
      SSLContext sslContext = SSLContext.getInstance("SSL");
      sslContext.init(null, trustManager, new SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sslContext
          .getSocketFactory());
      SocketFactory factory = sslContext.getSocketFactory();
      socket = (SSLSocket) factory.createSocket(hostname, port);
      socket.startHandshake();
    }
    catch (Exception e) {

    }
    return socket;
  }

  /**
   * Creates and returns an implementation of a <code>TrustManager</code> that
   * trusts all SSL certificates. This method is used in the initial handshake
   * to the target SSL server, so that its certificates can be extracted.
   * 
   * @return
   */
  private static X509TrustManager nullTrustManager() {
    return new X509TrustManager() {

      public void checkClientTrusted(X509Certificate[] certs, String authType) {
      }

      public void checkServerTrusted(X509Certificate[] certs, String authType) {
      }

      public X509Certificate[] getAcceptedIssuers() {
        return null;
      }
    };
  }

}
