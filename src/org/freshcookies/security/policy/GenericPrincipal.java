/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.policy;

import java.security.Principal;

/**
 * Simple principal class.
 * 
 * @author Andrew Jaquith
 * @version $Revision: 1.3 $ $Date: 2007/02/24 17:27:43 $
 */
public final class GenericPrincipal implements Principal {

  private final String name;

  private final int hashCode;

  /**
   * Constructs a new GenericPrincipal with a supplied name.
   * 
   * @param name the name of the Principal
   */
  public GenericPrincipal(String name) {
    this.name = name;
    this.hashCode = name.hashCode();
  }

  /**
   * @see java.lang.Object#equals(java.lang.Object)
   */
  public final boolean equals(Object obj) {
    if (obj instanceof GenericPrincipal) {
      return ((GenericPrincipal) obj).getName().equals(name);
    }
    return false;
  }

  /**
   * @see java.security.Principal#getName()
   */
  public final String getName() {
    return name;
  }

  /**
   * @see java.lang.Object#hashCode()
   */
  public final int hashCode() {
    return hashCode;
  }

  /**
   * @see java.lang.Object#toString()
   */
  public final String toString() {
    return "(GenericPrincipal=" + name + ")";
  }

}
