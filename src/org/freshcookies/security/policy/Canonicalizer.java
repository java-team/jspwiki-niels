/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.policy;

import java.io.File;
import java.io.FilePermission;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketPermission;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.AccessControlException;
import java.security.AccessController;
import java.security.CodeSource;
import java.security.Permission;
import java.security.Principal;
import java.security.PrivilegedAction;
import java.security.cert.Certificate;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;

/**
 * Utility class that canonicalizes Permissions and file paths, It will initializes a list of property substitutions that can be used when
 * constructing file paths. This class can optionally dump a list of system properties (and their
 * file path equivalents) to disk.
 * @author Andrew Jaquith
 *
 */
public class Canonicalizer {

  /** Static String that means "no codesource." */
  public static final String NO_CODESOURCE = "n/c";
  
  private static final String CODEBASE_FILE_PREFIX = "file:";

  private static final String CODEBASE_URL_PREFIX = "url:";

  /** 
   * Constructs a new Canonicalizer instance.
   */
  public Canonicalizer()
  {
    super();
  }
  
  /**
   * Formats a ProtectionDomain, substituting properties into
   * the CodeSource or path, as needed.
   * 
   * @param pd the protection domain
   * @param properties a Properties map containg key-value pairs of property names and
   * associated file paths
   * @return a revised CodeSource, with system properties substitutions as
   *         needed, or the same CodeSource if no properties match
   */
  public final CodeSource propertize(CodeSource codeSource, Properties properties) {
    // Bail if codesource location is null
    if (codeSource == null || codeSource.getLocation() == null) {
      return codeSource;
    }
    
    for (Iterator it = properties.keySet().iterator(); it.hasNext();) {
      String path = (String)it.next();
      String property = (String)properties.get(path);
      String cs = codeSource.getLocation().toExternalForm();
      try {
        // Does the codebase start with file: ?
        String value = CODEBASE_FILE_PREFIX + path;
        if (cs.startsWith(value)) {
          URL url = new URL(CODEBASE_FILE_PREFIX + "${" + property + "}"
              + cs.substring(value.length()));
          Certificate[] certs = codeSource.getCertificates();
          return new CodeSource(url, certs);
        }

        // Does the codebase start with url: ?
        value = CODEBASE_URL_PREFIX + path;
        if (cs.startsWith(value)) {
          URL url = new URL(CODEBASE_URL_PREFIX + "${" + property + "}"
              + cs.substring(value.length()));
          return new CodeSource(url, codeSource.getCertificates());
        }
      }
      catch (MalformedURLException e) {
        break;
      }
    }
    return codeSource;
  }
  
  /**
   * Substitutes properties into a given <code>FilePermission</code>'s
   * URL or path, as needed.
   * 
   * @param perm the permission
   * @param properties a Properties map containg key-value pairs of property names and
   * associated file paths
   * @return a String representing the <code>FilePermission</code>'s URL or
   *         file path, with system properties substitutions as needed
   */
  public final Permission propertize(Permission perm, Properties properties) {
    String target = perm.getName();
    
    if (perm instanceof FilePermission) {
      // See if we can substitute any properties
      for (Iterator it = properties.keySet().iterator(); it.hasNext();) {
    	String path = (String)it.next();
        String property = (String)properties.get(path);
        String value = path;
        if (target.startsWith(value)) {
          String newPath = "${" + property + "}" + target.substring(value.length());
          perm = new FilePermission(newPath, perm.getActions());
          break;
        }
      }
    }
    return perm;
  }

/**
   * Formats a CodeSource for use with a policy file. The format is identical to
   * that used in policy files. If the URL of the codesource ends with a file
   * separator (/ or \), a "-" will be appended. The formatter will insert
   * system properties (such as <code>${user.home}</code>) as needed.
   * 
   * @param cs the code source
   * @return the code source, formatted nicely as a string
   */
  public static final String format(CodeSource codeSource) {
    if (codeSource == null || codeSource.getLocation() == null) {
      return NO_CODESOURCE;
    }
    String cs = codeSource.getLocation().toExternalForm();
    if (cs.endsWith(File.separator)) {
      cs += "-";
    }
    return cs;
  }

  /**
   * Formats an array of Principals; the array may be <code>null</code>.
   * 
   * @param principals the array of Principals to formatted
   * @return a single String, with each Principal separated by a single space
   */
  public static final String format(Principal[] principals) {
    return format(principals, false);
  }
  
  /**
   * Formats an array of CachedPrincipals, optionally with line delimeters suitable
   * for a policy file. The array may be <code>null</code>.
   * 
   * @param principals the array of CachedPrincipals to formatted
   * @param forPolicy <code>true</code> if the string should be formatted for
   *          a policy file, with indents and a linebreak after each principal
   * @return a single String, with each Principal separated by a single space
   */
  public static final String format(Principal[] principals, boolean forPolicy) {
    String out = "-";
    if (principals != null) {
      out = "";
      for (int i = 0; i < principals.length; i++) {
        if (forPolicy) {
          out += "  principal " + format(principals[i]);
          if (i < (principals.length - 1)) {
            out += ',';
          }
          out += "\n";
        }
        else {
          out += format(principals[i]);
          if (i < (principals.length - 1)) {
            out += ' ';
          }
        }
      }
    }
    return out;
  }
  
  /**
   * Returns a Map of key/value pairs that correspond the subset of
   * System properties that correspond to valid file paths.
   * @return the map
   */
  public static final Properties getPathSubstitutions() {
    final Properties substitutions = new Properties();
    AccessController.doPrivileged(new PrivilegedAction() {
      public Object run() {
        Enumeration keys = System.getProperties().propertyNames();
        while (keys.hasMoreElements()) {
          String substitutionProperty = (String) keys.nextElement();
          if (!substitutionProperty.equals("file.separator")) {
            String path = System.getProperty(substitutionProperty);
            if (path.length() > 0) {
              // Try making a file out of it; if it exists, then dereference it
              File file = new File(path);
              if (file.exists()) {
                try {
                  path = file.getCanonicalPath();
                }
                catch (IOException e) {
                  System.err.println("Error de-referencing: " + e.getMessage());
                }
                substitutions.put(path, substitutionProperty);
              }
            }
          }
        }
        return null;
      }
    });
    return substitutions;
  }
  
  /**
   * Formats a CachedPrincipal for use with a policy file. The string returned
   * is the principal class, plus a space, followed by the principal's getName()
   * value in quotes.
   * 
   * @param principal the Principal to format
   * @return a nicely-formatted string representing the Principal
   */
  public static final String format(Principal principal) {
    return principal.getClass().getName() + ' ' + '\"' + principal.getName() + '\"';
  }
  
  /**
   * Formats a CachedPermission for use in a policy file or log file. The format is
   * identical to that used in policy files.
   * @param perm the Permission to format
   * @return a nicely-formatted string representing the Permission
   */
  public static final String format(Permission permission) {
    String out = permission.getClass().getName() + " ";
    String target = permission.getName();
    String actions = permission.getActions();

    boolean hasName = (target != null && target.length() != 0);
    boolean hasActions = (actions != null && actions.length() != 0);
    String formattedTarget;
    if (hasName) {
      formattedTarget = target.replaceAll("\"", "\\u0022");
      out += ("\"" + formattedTarget + "\"");
      if (hasActions) {
        out += (", ");
      }
    }
    if (hasActions) {
      out += ("\"" + actions + "\"");
    }
    return out;
  }
  
  /**
   * Canonicalizes a CodeSource.
   * @param cs the CodeSource
   * @param properties the properties to use for expansion
   * @return the CodeSource with its file path 
   * @throws IOException
   */
  public final CodeSource canonicalize(CodeSource codeSource, Properties properties) throws IOException {
    
    URL url = codeSource.getLocation();
    Certificate[] certs = codeSource.getCertificates();
    
    // Canonicalize the URL
    if (url != null) {
      String cs = url.getPath();
      // Trim path to remove file: prefix
      if (cs.startsWith(CODEBASE_FILE_PREFIX)) {
        cs = cs.substring(5);
      }
      else if (cs.startsWith(CODEBASE_URL_PREFIX)) {
        return codeSource;
      }
      
      // See if we need to substitute any properties
      for (Iterator it = properties.keySet().iterator(); it.hasNext();) {
        String path = (String)it.next();
        String property = (String)properties.get(path);
        // Does the codebase start with the property?
        String value = "${" + property + "}";
        if (cs.startsWith(value)) {
          if (!path.endsWith(File.separator) && (cs.length() <= value.length())) {
            path += File.separator;
          }
          cs = path + cs.substring(value.length());
          break;
        }
      }
     
      // Create new file with canonical path
      File file = new File(cs).getCanonicalFile();
      cs = file.getAbsolutePath();
      url = new URL("file:" + cs);
    }
    
    return new CodeSource(url, certs);
  }
  
  /**
   * <p>
   * Normalizes permissions by changing permission targets to canonical forms.
   * For example:
   * </p>
   * <ul>
   * <li>{@link java.io.FilePermission} paths are converted to canonical form
   * (symbolic links dereferenced), if possible</li>
   * <li>{@link java.net.SocketPermission} local host names 127.0.0.1 and ""
   * are converted to <code>localhost</code></li>
   * </ul>
   * <p>
   * All other permission types are returned unchanged.
   * </p>
   * @param permission the permission to canonicalize
   * @return the canonicalized permission
   * @throws UnknownHostException 
   */
  public final Permission canonicalize(Permission permission)
  {
    // For file permissions, canonicalize the file path
    if (permission instanceof FilePermission) {
      File file = new File(permission.getName());
      try {
        String canonicalName = file.getCanonicalPath();
        if (!permission.getName().equals(canonicalName)) {
          String actions = permission.getActions();
          permission = new FilePermission(canonicalName, actions);
        }
      }
      catch (IOException e) {
        System.err.println(e.getMessage());
      }
    }

    // For socket permissions, normalize "localhost" refs
    else if (permission instanceof SocketPermission) {
      String host = permission.getName();
      if (host.startsWith("localhost")) {
        return permission;
      }
      
      // Normalize
      String port = null;
      String target = permission.getName();
      int lastColon = target.lastIndexOf(":");
      if (lastColon > -1)
      {
        host = target.substring(0,lastColon);
        port = target.substring(lastColon + 1, target.length());
      }
      
      final String testHost = host;
      String resolvedHost = (String)AccessController.doPrivileged(new PrivilegedAction() {
        public Object run() {
          try {
            InetAddress addr = InetAddress.getByName(testHost);
            if (addr.isLoopbackAddress())
            {
              return "localhost";
            }
          }
          catch (AccessControlException e)
          {
            // If we don't have permission to resolve the host, it's ok
            return testHost;
          }
          catch (UnknownHostException e) {
            // It's ok if we can't resolve the host...
            return testHost;
          }
          return testHost;
        }
      },null);

      target = (port == null) ? resolvedHost : resolvedHost + ":" + port;
      String actions = permission.getActions();
      permission = new SocketPermission(target, actions);
    }
    
    // All other permissions are unchanged
    return permission;
  }
  
}
