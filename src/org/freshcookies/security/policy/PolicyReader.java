/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.policy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.AccessControlException;
import java.security.AccessController;
import java.security.CodeSource;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.security.Principal;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.ProtectionDomain;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses a Java security policy file into memory.
 * <p>If a SecurityManager is running, the security policy must grant 
 * PolicyReader's CodeSource the following permissions:</p>
 * <ul>
 *   <li><code>java.io.FilePermission "${java.home}/lib/security/java.policy", "read"</code></li>
 *   <li><code>java.io.FilePermission "${user.home}/.java.policy", "read"</code></li>
 *   <li><code>java.io.FilePermission "<em>path-to-policy-file</em>", "read"</code></li>
 *   <li><code>java.io.FilePermission "<em>path-to-keystore</em>", "read";</code></li>
 *   <li><code>java.util.PropertyPermission "file.encoding", "read"</code></li>
 *   <li><code>java.util.PropertyPermission "java.security.policy", "read"</code></li>
 *   <li><code>java.util.PropertyPermission "java.home", "read"</code></li>
 *   <li><code>java.lang.RuntimePermission "accessClassInPackage.java.,javax."</code></li>
 *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-permission-packages</em>"</code></li>
 *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-principal-packages</em>"</code></li>
 *   <li><code>java.lang.RuntimePermission "getProtectionDomain"</code></li>
 *   <li><code>java.net.NetPermission "specifyStreamHandler"</code></li>
 * </ul>
 * <p>...where <em>path-to-policy-file</em> is the policy file to parse, 
 * <em>path-to-keystore</em> is the path of the keystore named in the policy file (if any), 
 * and <em>custom-permission-packages</em> and <em>custom-principal-packages</em> 
 * denote the names of custom {@link java.security.Permission} and 
 * {@link java.security.Principal} classes to be loaded by the current classloader when the
 * file is parsed. The methods that require these permissions enclose their operations inside
 * of <code>doPrivileged</code> blocks, so calling classes' ProtectionDomains do <em>not</em>
 * need to be granted these privileges.</p>
 * 
 * @author Andrew Jaquith
 * @version $Revision: 1.5 $ $Date: 2007/04/09 02:34:52 $
 */
public class PolicyReader {

	private static final Pattern COMMENTS_PATTERN = Pattern.compile(
			"^\\s*\\/\\/.*$", Pattern.MULTILINE);

	private static final Pattern KEYSTORE_PATTERN = Pattern.compile(
			"^keystore \"(.*?)\";.*$", Pattern.CASE_INSENSITIVE);

	private static final Pattern GRANT_PATTERN = Pattern
			.compile("grant(.*?) \\{ *(.*?) *\\};");

	private static final Pattern LINEBREAKS_PATTERN = Pattern.compile(
			"[\\n|\\r\\n|\\u0085|\\u2028|\\u2029]", Pattern.MULTILINE);

	private static final String PROP_JAVA_HOME = "java.home";

	private static final String PROP_USER_HOME = "user.home";

	private static final String PROP_JAVA_SECURITY_POLICY = "java.security.policy";

	private static final String REGEX_COMMA_DELIMITER = " *, *";

	private static final String REGEX_SEMICOLON_DELIMITER = " *; *";

	private static final String TOKEN_CODEBASE = "codeBase";

	private static final String TOKEN_PRINCIPAL = "principal";

	private static final String TOKEN_PERMISSION = "permission";

	private static final String TOKEN_SIGNEDBY = "signedBy";

	private static final String DOUBLE_QUOTE = "\"";

	private static final String ONE_SPACE = " ";

	private static final String WHITESPACE = "\\s+";

	private static final int NOT_FOUND = -1;

	private final File policy;

	private final List domains;
	
	private KeyStore keystore = null;

	private File keystoreFile = null;

	private String policyString = null;

	private final List exceptions = new ArrayList();

	private SecurityTokenFactory tokenFactory = null;

	private boolean validPolicy = false;
  
  private final String charset;

	/**
	 * <p>
	 * Constructs a new PolicyReader for parsing a supplied policy File using the
   * Java platform standard charset.
	 * @param file
	 *            the policy file to be parsed
	 * @throws FileNotFoundException
	 *            if the policy file does not exist in the filesystem
	 * @throws SecurityException if the running SecurityManager denied any of the required
	 * Permissions
	 */
	public PolicyReader(File file) throws FileNotFoundException {
    this(file, (String)AccessController.doPrivileged( new PrivilegedAction() { 
      public Object run() { return System.getProperty("file.encoding"); } }));
	}

  /**
   * <p>
   * Constructs a new PolicyReader for parsing a supplied policy File. If the
   * file does not exist, this constructor returns an
   * {@linkplain IllegalArgumentException}.
   * </p>
     * <p>If a SecurityManager is running, the security policy must grant 
     * PolicyReader's CodeSource the following permissions:</p>
     * <ul>
   *   <li><code>java.io.FilePermission "<em>path-to-policy-file</em>", "read"</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.java.,javax."</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-permission-packages</em>"</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-principal-packages</em>"</code></li>
     *   <li><code>java.net.NetPermission "specifyStreamHandler"</code></li>
     * </ul>
     * <p>...where <em>custom-permission-packages</em> and <em>custom-principal-packages</em> 
     * are the names of custom {@link java.security.Permission} and 
     * {@link java.security.Principal} classes loaded from the current classloader.</p>
   * 
   * @param file
   *            the policy file to be parsed
   * @throws FileNotFoundException
   *            if the policy file does not exist in the filesystem
   * @throws SecurityException if the running SecurityManager denied any of the required
   * Permissions
   */
  public PolicyReader(File file, String charset) throws FileNotFoundException {
    super();
    this.policy = file;
    this.domains = new ArrayList();
    this.charset = charset;
    boolean exists = secureExists(policy);
    if (!exists) {
      throw new IllegalArgumentException("File " + policy
          + " does not exist, or the SecurityManager prohibited access to it.");
    }
    
    // Set up the token factory; if the SecurityManager prohibits it, then propagate the exception
    this.tokenFactory = 
      (SecurityTokenFactory)AccessController.doPrivileged(new PrivilegedAction() {
        public Object run() throws SecurityException {
          return new SecurityTokenFactory(new URL[0]);
        }
      
      });
  }
  
	/**
	 * Examines a Keystore and returns the alias that matches a given signing
	 * Certificate. If no match is found, this method returns <code>null</code>.
	 * 
	 * @param ks
	 *            the keystore to search
	 * @param cert
	 *            the certificate to match
	 * @return the alias corresponding to the certificate
	 */
	public static String findAlias(KeyStore ks, Certificate cert) {
		try {
			Enumeration aliases = ks.aliases();
			while (aliases.hasMoreElements()) {
				String alias = (String) aliases.nextElement();
				if (ks.isKeyEntry(alias)) {
					Certificate keystoreCert = ks.getCertificate(alias);
					if (cert.equals(keystoreCert)) {
						return alias;
					}
				}
			}
		} catch (KeyStoreException e) {
			System.err.println(e.getMessage());
		}
		return null;
	}

	/**
	 * Returns the ProtectionDomains parsed by the {@link #read()} method.
	 * If the {@link #read()} method has not been called yet, this method 
	 * will return a zero-length array. 
	 * @return the ProtectionDomains parsed from the policy file
	 */
	public ProtectionDomain[] getProtectionDomains() {
		return (ProtectionDomain[])domains.toArray(new ProtectionDomain[domains.size()]);
	}

	/**
	 * <p>
	 * Static method that identifies the active security policies for the JVM
	 * and returns an array of PolicyReader objects (one for each active
	 * policy).
	 * </p>
	 * <p>
	 * This method accesses the file system to determine whether certain default
	 * security policies exist, and it reads the <code>java.home</code> and
	 * <code>user.home</code> properties to determine the location of the
	 * system and user security policies. Therefore, to run this method under a
	 * SecurityManager, the security policy must grant PolicyReader's 
	 * CodeSource the following permissions:</p>
	 * </p>
	 * <ul>
	 *   <li><code>java.util.PropertyPermission "java.home", "read"</code></li>
	 *   <li><code>java.util.PropertyPermission "java.security.policy", "read"</code></li>
	 *   <li><code>java.util.PropertyPermission "user.home", "read"</code></li>
	 *   <li><code>java.io.FilePermission "${java.home}/lib/security/java.policy", "read"</code></li>
	 *   <li><code>java.io.FilePermission "${user.home}/.java.policy", "read"</code></li>
	 * </ul>
	 * 
	 * @return PolicyReader objects for each active policy file
	 * @throws SecurityException if the running SecurityManager denied any of the required
	 * Permissions
	 */
	public static PolicyReader[] findPolicies() {
		final Set files = new HashSet();
		final String fs = File.separator;

		AccessController.doPrivileged(new PrivilegedAction() {

			public Object run() {
				// Add the system policy if it exists (it should!)
				File systemPolicy = new File(System.getProperty(PROP_JAVA_HOME)
						+ fs + "lib" + fs + "security" + fs + "java.policy");
				if (systemPolicy.exists()) {
					files.add(systemPolicy);
				}

				// Add the user policy if it exists
				File userPolicy = new File(System.getProperty(PROP_USER_HOME)
						+ fs + ".java.policy");
				if (userPolicy.exists()) {
					files.add(userPolicy);
				}

				// Add the file pointed at by the policy system property if it
				// exists
				String policyProp = System
						.getProperty(PROP_JAVA_SECURITY_POLICY);
				if (policyProp != null) {
					if (policyProp.startsWith("file:")) {
						policyProp = policyProp.substring(5);
					}
					if (policyProp.length() > 0) {
						File propertyPolicy = new File(policyProp);
						if (secureExists(propertyPolicy)) {
							files.add(propertyPolicy);
						}
					}
				}
				return null;
			}
		});

		// Now, create policy readers for each file we found
		List readers = new ArrayList();
		for (Iterator it = files.iterator(); it.hasNext();) {
			try {
				readers.add(new PolicyReader((File) it.next()));
			}
			catch (FileNotFoundException e) {
				// Didn't find the policy file/denied access? Ok, just skip it
			}
		}
		return (PolicyReader[])readers.toArray(new PolicyReader[readers.size()]);
	}

	/**
	 * Static method that returns the certificate used to sign a particular
	 * class. We determine the signing certificate by obtaining the class'
	 * {@link java.security.ProtectionDomain} first, then looking up the
	 * CodeSource and extracting certificates. If the code source was not
	 * signed, this method returns <code>null</code>. This method obtains the
	 * ProtectionDomain of the class. Therefore, to run this method under a
	 * SecurityManager, the security policy must grant PolicyReader's 
	 * CodeSource the following permission:</p>
	 * </p>
	 * <ul>
	 *   <li><code>java.lang.RuntimePermission "getProtectionDomain"</code></li>
	 * </ul>
	 * 
	 * @param clazz the class whose signer should be returned
	 * @return the signing certificate, or <code>null</code> if no signer
	 * @throws SecurityException
	 *             if the running SecurityManager denied this ProtectionDomain 
	 *             the permission <code>java.lang.RuntimePermission "getProtectionDomain"</code>
	 */
	public static Certificate getSigner(final Class clazz) {
		ProtectionDomain pd = null;

		pd = (ProtectionDomain) AccessController
				.doPrivileged(new PrivilegedAction() {
					public Object run() {
						return clazz.getProtectionDomain();
					}
				});

		if (pd != null) {
			CodeSource cs = pd.getCodeSource();
			if (cs != null) {
				Certificate[] certs = cs.getCertificates();
				if (certs != null && certs.length > 0) {
					return certs[0];
				}
			}
		}
		return null;
	}

	/**
	 * Returns <code>true</code> if a class is digitally signed. This looks up
	 * the signer by delegating to {@link #getSigner(Class)}, and therefore
	 * requires the same privileges if run under a SecurityManager.
	 * 
	 * @param clazz
	 *            the class to check
	 * @return the result.
	 * @throws SecurityException
	 *             if the running SecurityManager denied this ProtectionDomain 
	 *             the permission <code>java.lang.RuntimePermission "getProtectionDomain"</code>
	 * @see #getSigner(Class)
	 */
	public static boolean isSigned(Class clazz) {
		Certificate cert = getSigner(clazz);
		return (cert != null);
	}

	/**
	 * Returns the File object used to instantiate this PolicyReader.
	 * 
	 * @see #PolicyReader(File)
	 * @return the file
	 */
	public File getFile() {
		return policy;
	}

	/**
	 * <p>
	 * Returns the keystore associated with the policy file, if one is specified
	 * in the policy. The strategy used to file the keystore follows the J2SE
	 * convention: if the file named in the <code>keystore</code> line of the
	 * policy file is not an absolute path name, the location for the file is
	 * assumed to be the same directory as the keystore. If the keystore cannot
	 * be located, this method throws an {@link java.io.IOException}.
	 * </p>
	 * 
	 * <p>
	 * If the policy file does not specify a keystore, this method returns
	 * <code>null</code>, and
	 * <em>calling methods should check for this result!</em>
	 * </p>
	 * <p>
	 * This method attempts to read the keystore into memo from the file
	 * specified in the secuity policy. Therefore, to run this method under a
	 * SecurityManager, the security policy must grant PolicyReader's 
	 * CodeSource the following permission:</p>
	 * </p>
	 * <ul>
	 *   <li><code>java.io.FilePermission "<em>/path-to-keystore</em>", "read"</code></li>
	 * </ul>
	 * 
	 * @return the keystore associated with the policy file, or
	 *         <code>null</code> if not found
	 * @throws IOException
	 *             if the keystore file cannot be read from the filesystem
	 *             because it does not exist
	 * @throws SecurityException if the running SecurityManager denied the Permission
	 * <code>java.io.FilePermission "<em>path-to-keystore</em>", "read"</code>
	 */
	public KeyStore getKeyStore() throws IOException {

		if (keystore == null) {

			if (policyString == null) {
				loadPolicy(policy);
			}

			// Get keystore, it if exists
			Matcher matcher = KEYSTORE_PATTERN.matcher(policyString);
			if (matcher.matches()) {
				String match = matcher.group(1);
				keystoreFile = new File(match);
				if (!keystoreFile.isAbsolute()) {
					keystoreFile = new File(policy.getParentFile(), match);
					if (!secureExists(keystoreFile)) {
						throw new IOException("Couldn't find keystore "
								+ keystoreFile + ", or the SecurityManager prohibited access to it.");
					}
				}
			}

			// If the keystore file is not null, load the keystore itself
			if (keystoreFile != null) {
				try {
					AccessController.doPrivileged(new PrivilegedExceptionAction() {
						public Object run() throws IOException {
							try {
								FileInputStream is = new FileInputStream(keystoreFile);
								keystore = KeyStore.getInstance(KeyStore.getDefaultType());
								keystore.load(is, null);
								return null;
							} catch (CertificateException e) {
								throw new IOException(e.getMessage());
							} catch (KeyStoreException e) {
								throw new IOException(e.getMessage());
							} catch (NoSuchAlgorithmException e) {
								throw new IOException(e.getMessage());
							} catch (SecurityException e) {
								return new IOException("Security manager prohibited read access to "
									+ keystoreFile.getAbsolutePath());
							}
						}
					});
				} catch (PrivilegedActionException e) {
					throw (IOException) e.getException();
				}
			}
		}
		return keystore;
	}

	/**
	 * <p>
	 * Parses the security policy file, and loads its contents into memory. Any
	 * errors encountered while reading the policy will be added to an
	 * internally cached set of error messages, which may be obtained by
	 * {@link #getMessages()}.
	 * </p>
	 * <p>
	 * Because this method attempts to read the policy file contents, it
	 * requires <code>read</code> permissions to the file containing the
	 * policy if a SecurityManager is running. It will also require
	 * <code>read</code> access to the keystore named in the policy, and will
	 * need to read named classes' ProtectionDomains to verify signatures if
	 * required by the policy. Therefore, to run this method under a
	 * SecurityManager, the security policy must grant PolicyReader's 
	 * CodeSource the following permission:</p>
     * <ul>
     *   <li><code>permission java.io.FilePermission "<em>path-to-policy-file</em>", "read"</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.java.,javax."</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-permission-packages</em>"</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-principal-packages</em>"</code></li>
     *   <li><code>java.lang.RuntimePermission "createClassLoader"</code></li>
	 *   <li><code>java.lang.RuntimePermission "getProtectionDomain"</code></li>
	 *   <li><code>java.net.NetPermission "specifyStreamHandler"</code></li>
     * </ul>
	 * 
	 * @throws IOException if the policy file cannot be read
	 * @throws SecurityException when the SecurityManager, if running, denies 
	 * the ProtectionDomain of this class any of the required permissions
	 */
	public void read() throws IOException {

		if (policyString == null) {
			loadPolicy(policy);
			getKeyStore();
		}

		// Parse the grants
		domains.clear();
		Matcher m = GRANT_PATTERN.matcher(policyString);
		while (m.find()) {
			String granteeString = m.group(1).trim();
			String permissionString = m.group(2).trim();
			ProtectionDomain domain = parseProtectionDomain(granteeString, permissionString);
			domains.add(domain);
		}

		if (exceptions.size() > 0) {
			System.err.println("The parser returned these errors:");
			for (Iterator it = exceptions.iterator(); it.hasNext();) {
				Exception e = (Exception) it.next();
				System.err.println(e.getMessage());
			}
		} else {
			// Hooray! Everything parsed nicely.
			validPolicy = true;
		}

	}

	/**
	 * The parsing errors encountered by PolicyReader when the {@link #read()} method
	 * was last invoked. The list of errors is reset every time <code>read()</code>
	 * is called.
	 * @return the list of Exception objects
	 */
	public List getMessages() {
		return Collections.unmodifiableList(exceptions);
	}
	
	/**
	 * Adds an Exception to the list of parsing errors.
	 * @param e the exception added
	 */
	protected void addMessage(Exception e) {
		exceptions.add(e);
	}

	/**
	 * Protected method that loads the policy file into memory, scrubs contents
	 * of line breaks and extra whitespace, and returns it as a string. If
	 * running under a SecurityManager, this method must have read access to the
	 * file.
	 * 
	 * @param file the policy file to load
	 * @throws IOException
	 *             if the file does not exist in the file system
	 * @throws SecurityException if the running SecurityManager denied the Permission
	 * <code>java.io.FilePermission "<em>path-to-file</em>", "read"</code>
	 */
	protected void loadPolicy(final File file) throws IOException {
		StringBuffer contents = new StringBuffer();
		BufferedReader reader = null;
		try {
			reader = (BufferedReader) AccessController
					.doPrivileged(new PrivilegedExceptionAction() {
						public Object run() throws SecurityException, UnsupportedEncodingException {
							try {
								BufferedReader in = new BufferedReader(
										new InputStreamReader( new FileInputStream(file), charset ) );
								return in;
							} catch (FileNotFoundException e) {
								return null;
							}
						}
					});
		} catch (PrivilegedActionException e) {
			throw new SecurityException("Could not open policy "
					+ file.getAbsolutePath()
					+ "; access was denied by the SecurityManager.");
		}

		// If reader was null, it means we could not find it in the filesystem
		if (reader == null) {
			throw new IOException("Could not open policy "
					+ file.getAbsolutePath() + "; it does not exist.");
		}

		// Read the file and append its contents to a string buffer
		int ch;
		while ((ch = reader.read()) != NOT_FOUND) {
			contents.append((char) ch);
		}
		reader.close();

		// Scrub out all comments
		Matcher matcher = COMMENTS_PATTERN.matcher(contents);
		String contentsNoComments = matcher.replaceAll(ONE_SPACE);

		// Replace line breaks with spaces
		matcher = LINEBREAKS_PATTERN.matcher(contentsNoComments);
		String contentsNoLineBreaks = matcher.replaceAll(ONE_SPACE);

		// Replace all multi-character whitespace with one space
		policyString = contentsNoLineBreaks.replaceAll(WHITESPACE, ONE_SPACE)
				.trim();
	}

	/**
	 * <p>
	 * Returns a valid ProtectionDomain by parsing a <code>grant</code> block 
	 * that denotes the codebase, pricipals and signers to whom permissions 
	 * should be granted. The <code>grant</code> block is comprised of two parts,
	 * which are passed to this method: the <em>grantee</em> string and the
	 * <em>permissions</em> string.
	 * </p>
	 * </p>
	 * The <em>grantee</em> is the codebase, principals and signers portion
	 * of the <code>grant</code> block, minus the word "grant" itself. 
	 * The following are all valid grantee strings:
	 * </p>
	 * <pre> codeBase "file:${user.home}/workspace/freshcookies-security/target-test/"
	 * signedBy "testsigner", principal org.freshcookies.security.policy.GenericPrincipal "Foo", principal "Bar"
	 * signedBy "testsigner"</pre>
	 * <p>
	 * The grantee string is processed as follows:
	 * </p>
	 * <ul>
	 *   <li>If the grantee strings contains a <code>codeBase</code> token, this method
	 * will attempt to create a {@link java.security.CodeSource} object and
	 * stash it in the returned Grantee. It will not actually verify that the
	 * CodeSource exists, however.</li>
	 *   <li>If the grantee line contains a <code>signedBy</code> token, this method
	 * will iterate through the aliases in the Keystore returned by
	 * {@link #getKeyStore()} and attempt to find a matching signer alias. If no
	 * match, an error message is added to the internal list.</li>
	 * </ul>
	 * <p>
	 * The permission string is the portion of the <code>grant</code> statement
	 * between braces (<code>{}</code>). Permissions are delimited with a semicolons.
	 * The permissions string is processed as follows:
	 * </p>
	 * <ul>
	 *   <li>Each Permission is parsed into the correct class name, targets and actions.</li>
	 *   <li>After the class name is determined, this method will attempt
	 * to load the specified permissions into memory using the current ClassLoader.</li>
	 *   <li>If a particular permission statement contains a <code>signedBy</code>
	 * token, this method will also attempt to verify that the Permission was 
	 * signed by the keystore alias specified in the statement.</li>
	 * </ul>
	 * <p>The ProtectionDomain that is returned by this method will be constructed with
	 * CodeSource, PermissionCollection, and Principal array parameters that correspond
	 * to those parsed from the grantee and permission strings. The ClassLoader passed
	 * to {@link java.security.ProtectionDomain#ProtectionDomain(CodeSource, PermissionCollection, ClassLoader, Principal[])}
	 * will be the current ClassLoader; that is, the one that 
	 * <p>
	 * This method needs certain privileges to run correctly under a
	 * SecurityManager. It requires the NetPermission
	 * <code>specifyStreamHander</code> to construct CodeSource URLs. It will
	 * also require permission to obtain the ProtectionDomain for loaded classes.
	 * For example, an appropriate grant statement might look like this:
	 * </p>
	 * <pre>grant codeBase "file:/<em>path-to-this-jar</em>/freshcookies-security-<em>version</em>.jar" {
	 *   permission java.lang.RuntimePermission "getProtectionDomain";
	 *   permission java.net.NetPermission "specifyStreamHandler";
	 *};</pre>
	 * 
	 * @param granteeString
	 *            the grantee string, which is the portion of the grant block
	 *            between the <code>grant</code> token and the opening brace (<code>{</code>)
	 *            that begins the individual permission lines
	 * @param permissionString
	 *            the permission string, which is the portion of the grant block
	 *            between opening and closing braces(<code>{}</code>). This string will 
	 *            be split into individual permissions and parsed
	 * @return the initialized ProtectionDomain
	 * @throws SecurityException when the SecurityManager, if running, denies 
	 * the protection domain of this class any of the following permissions:
     * <ul>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.java.,javax."</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-principal-packages</em>"</code></li>
     *   <li><code>java.lang.RuntimePermission "createClassLoader"</code></li>
	 *   <li><code>java.lang.RuntimePermission "getProtectionDomain"</code></li>
	 *   <li><code>java.net.NetPermission "specifyStreamHandler"</code></li>
     * </ul>
	 */
	protected ProtectionDomain parseProtectionDomain(String granteeString, String permissionString) {
		
		// Grantee variables: codebase, signers, principals
		URL codeBaseUrl = null;
		Certificate[] signers = null;
		List principalsList = new ArrayList();

		// Split the grantee string into tokens: one or more of codebase, signedby, principal blocks
		String[] granteeStrings = granteeString.split(REGEX_COMMA_DELIMITER);
		
		// Parse each token
		for (int i = 0; i < granteeStrings.length; i++) {
			String grantee = granteeStrings[i].trim();

			// Extract CodeBase URL, and construct
			if (grantee.startsWith(TOKEN_CODEBASE)) {
				codeBaseUrl = extractCodeBaseUrl(grantee);
			}

			// Extract signing certificate alias, and resolve in keystore
			else if (grantee.startsWith(TOKEN_SIGNEDBY)) {
				signers = extractSigningCertificates(grantee);
			}

			// Extract principals, and instantiate
			else if (grantee.startsWith(TOKEN_PRINCIPAL)) {
				Principal principal = extractPrincipal(grantee);
				if (principal != null) {
					principalsList.add(principal);
				}
			}
		}

		// Ok, now build a CodeSource out of the URL and cert alias
		// We need the certs at some point...
		CodeSource codeSource = new CodeSource(codeBaseUrl, signers);

		// Turn our Principal list into an array (but zero-length means we just set it as null)
		Principal[] principals = (Principal[]) principalsList.toArray(new Principal[principalsList.size()]);
		if (principals.length == 0) {
			principals = null;
		}
		
		// Extract the Permissions assigned to the grantee and lock it
		PermissionCollection permissions = extractPermissionCollection(permissionString);
    permissions.setReadOnly();
		
		// Create and return the ProtectionDomain
		ClassLoader classLoader = PolicyReader.class.getClassLoader();
		ProtectionDomain pd = new ProtectionDomain(codeSource, permissions, classLoader, principals);
		return pd;
	}

	/**
	 * Returns <code>true</code> if a supplied File exists in the file system, and
	 * the SecurityManager (if running) has granted read access to it.
	 * @param file the file to check
	 * @return the result
	 * @throws SecurityException if the running SecurityManager denied the Permission
	 * <code>java.io.FilePermission "<em>path-to-file</em>", "read"</code>
	 */
	protected static boolean secureExists(final File file) throws SecurityException {
		try {
			Boolean exists = (Boolean) AccessController.doPrivileged(
				new PrivilegedExceptionAction() {
					public Object run() throws SecurityException {
						return Boolean.valueOf(file.exists());
					}
				});
			return exists.booleanValue();
		}
		catch (PrivilegedActionException e) {
			throw (SecurityException)e.getException();
		}
	}
	
	/**
	 * Extracts the URL from a <code>codeBase</code> token string. 
	 * Example: <code>codeBase "file:${user.home}/workspace/freshcookies-security/target/classes/-"</code>.
	 * @param token the string containing the URL
	 * @return the constructed URL
	 */
	protected URL extractCodeBaseUrl(String token) {
		URL url = null;
		try {
			final String s = extractTarget(token, TOKEN_CODEBASE);
			url = (URL) AccessController.doPrivileged(new PrivilegedExceptionAction() {
				public Object run() throws MalformedURLException {
					return new URL(s);
				}
			});
		} catch (PolicyException e) {
			addMessage(e);
		} catch (PrivilegedActionException e) {
			addMessage(e.getException());
		}
		return url;
	}
	
	/**
	 * Extracts the Principal from a <code>principal</code> token string. 
	 * Example: <code>principal org.freshcookies.security.policy.GenericPrincipal "Andrew Jaquith"</code>.
	 * @param token the string containing the Principal
	 * @return the constructed Principal
	 */
	protected Principal extractPrincipal(String token) {
		Principal principal = null;
		final String s = token.substring(TOKEN_PRINCIPAL.length()).trim();
		try {
			principal = (Principal)AccessController.doPrivileged(new PrivilegedExceptionAction() {
				public Object run() throws AccessControlException, ClassNotFoundException {
					return tokenFactory.getPrincipal(s);
				}
			});
		}
		catch (PrivilegedActionException e) {
			addMessage(e.getException());
		}
		return principal;
	}
	
	/**
	 * Extracts and resolves signing Certificates from a <code>signedBy</code> token string. 
	 * Example: <code>signedBy "testsigner"</code>.
	 * @param token the string containing the certificate alias
	 * @return the resolved certificate chain
	 */
	protected Certificate[] extractSigningCertificates(String token) {
		Certificate[] signers = null;
		
		// If keystore file is null, then signedby doesn't mean anything
		if (keystoreFile == null) {
			addMessage(new PolicyException(
					"A 'signedBy' entry exists, but no keystore was specified; ignoring."));
		} else {
			try {
				String signedBy = extractTarget(token, TOKEN_SIGNEDBY);
				signers = getKeyStore().getCertificateChain( signedBy);

				// It's an error if we can't find the 'signedBy' alias
				if (signers == null) {
					addMessage(new PolicyException(
							"Certificate with alias '" + signedBy
									+ "' not found in keystore "
									+ keystoreFile));
				}
			} catch (Exception e) {
				addMessage(e);
			}
		}
		return signers;
	}
	
	/**
	 * <p>Parses permissions from a <code>grant</code> block and returns a PermissionCollection.
	 * The permissions are extracted from the string portion of the <code>grant</code> 
	 * statement between braces (<code>{}</code>). Permissions are delimited with a semicolons.
	 * This method will attempt to load the specified permissions into memory using the
	 * current ClassLoader. If a particular permission statement contains a 
	 * <code>signedBy</code> token, this method will also attempt to verify that the 
	 * Permission was signed by the keystore alias specified in the statement.</p>
	 * @param permissions the permissions string, which will be parsed into individual permissions
	 * and parsed. For example, the following string will be parsed as two separate permissions:
	 * <pre> permission java.util.PropertyPermission "java.version", "read";
	 * permission java.util.PropertyPermission "java.vendor", "read";</pre>
	 * @return the instantiated collection of Permissions for the Grantee
	 * @throws SecurityException when the SecurityManager, if running, denies 
	 * the protection domain of this class any of the following permissions:
     * <ul>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.java.,javax."</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-permission-packages</em>"</code></li>
     *   <li><code>java.lang.RuntimePermission "createClassLoader"</code></li>
	 *   <li><code>java.lang.RuntimePermission "getProtectionDomain"</code></li>
     * </ul>
     * @param the 
	 */
	protected PermissionCollection extractPermissionCollection(String permissions) {
		String[] p = permissions.split(REGEX_SEMICOLON_DELIMITER);
		PermissionCollection permissionCollection = new Permissions();

		for (int j = 0; j < p.length; j++) {

			String item = p[j].trim();
			if (item.startsWith(TOKEN_PERMISSION)) {

				// Look and instantiate the Permisson
				final String permissionString = item.substring(
						TOKEN_PERMISSION.length()).trim();
				Permission permission = null;
				try {
					permission = (Permission)AccessController.doPrivileged(new PrivilegedExceptionAction(){
						public Object run() throws AccessControlException, ClassNotFoundException {
							return tokenFactory.getPermission(permissionString);
						}
					});

					// Verify signature if there is a 'signedBy' clause
					try {
						boolean isSigned = (permissionString
								.indexOf(TOKEN_SIGNEDBY) != -1);
						if (isSigned && permission != null) {
							if (!isVerified(permission.getClass())) {
								addMessage(new PolicyException(
										"Could not verify permission class signature: "
												+ permissionString));
							}
						}
					} catch (IOException e) {
						addMessage(new PolicyException(
								"Could not instantiate permission: "
										+ permissionString));
					}
					// Add the newly instantiated permission to the grants
					// collection
					permissionCollection.add(permission);
					
				} catch (PrivilegedActionException e) {
					addMessage(e.getException());
				}
			}
		}
		permissionCollection.setReadOnly();
		return permissionCollection;
	}

	/**
	 * Returns <code>true></code> if the security policy file was parsed
	 * correctly and validated without errors. If the {@link #read()} method has
	 * not yet been called, this method returns <code>false</code>.
	 * 
	 * @return <code>true</code> if the policy file parsed correctly,
	 * <code>false</code> otherwise
	 */
	public boolean isValid() {
		return validPolicy;
	}

	/**
	 * Returns <true>code</code> if a supplied class' {@link CodeSource} is
	 * digitally signed, and the certificate used to sign it can be found in
	 * this <code>PolicyReader</code>'s keystore. If the CodeSource is not
	 * signed, or if a matching certificate cannot be found, this method returns
	 * <code>false</code>.
	 * 
	 * @param clazz
	 *            the class to verify
	 * @return the result of the verification operation
	 * @throws IOException
	 *             if the keystore cannot be read
	 * @throws SecurityException
	 *             if the running SecurityManager denied this ProtectionDomain 
	 *             the permission <code>java.lang.RuntimePermission "getProtectionDomain"</code>
	 * @see #getKeyStore()
	 */
	public boolean isVerified(Class clazz) throws IOException {
		Certificate cert = getSigner(clazz);
		try {
			Enumeration aliases = getKeyStore().aliases();
			while (aliases.hasMoreElements()) {
				String alias = (String) aliases.nextElement();
				if (keystore.isKeyEntry(alias)) {
					Certificate keystoreCert = keystore.getCertificate(alias);
					if (cert.equals(keystoreCert)) {
						return true;
					}
				}
			}
		} catch (KeyStoreException e) {
			throw new IOException(e.getMessage());
		}
		return false;
	}

	/**
	 * Extracts a substring starting after a given start string. The start
	 * string <em>must</em>, in fact, start the overall string, and the
	 * extracted string must be surrounded by double quotes. For example, if
	 * this method is called with parameters <code>signedBy "testsigner"</code>
	 * and <code>signedBy</code>, it will return <code>testsigner</code>.
	 * 
	 * @param s
	 *            the string from which to extract the substring
	 * @param startAfter
	 *            the start string; if <code>null</code>, the start point
	 *            will be position 0
	 * @return the extracted substring
	 * @throws PolicyException
	 *             if the string is not well-formed
	 */
	private static String extractTarget(String s, String startAfter)
			throws PolicyException {
		String s2 = (startAfter == null) ? s : s.substring(startAfter.length())
				.trim();
		if (s2.startsWith(DOUBLE_QUOTE) && s2.endsWith(DOUBLE_QUOTE)
				&& s2.length() > 2) {
			return s2.substring(1, s2.length() - 1);
		}
		throw new PolicyException("Policy string \"" + s
				+ "\" must be at least one character and surrounded by quotes.");
	}

}
