/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.policy;

import java.io.Serializable;
import java.security.Principal;
import java.text.Collator;
import java.util.Comparator;

/**
 * Comparator class for sorting objects of type Principal.
 * Used for sorting arrays or collections of Principals.
 * The principals are sorted according to their name values. If
 * the names are the same, the principal with the first class name
 * wins.
 * @author Andrew Jaquith
 * @version $Revision: 1.1 $ $Date: 2007/02/24 17:27:43 $
 */
public class PrincipalComparator 
    implements Comparator, Serializable 
{
    private static final long serialVersionUID = 1L;

    /** Constructs a new PrincipalComparator. */
    public PrincipalComparator() {
    	super();
    }
    
    public int compare( Object o1, Object o2 )
    {
        Collator collator = Collator.getInstance();
        if ( o1 instanceof Principal && o2 instanceof Principal )
        {
            int comparison = collator.compare( ((Principal)o1).getName(), ((Principal)o2).getName() );
            if ( comparison == 0 )
            {
              return collator.compare(o1.getClass().getName(), o2.getClass().getName());
            }
            return comparison;
        }
        throw new ClassCastException( "Objects must be of type Principal.");
    }
      
}
