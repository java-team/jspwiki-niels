/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.policy;

/**
 * Lightweight checked Exception subclass for propagating security policy parsing errors.
 * @author Andrew Jaquith
 * @version $Revision: 1.2 $ $Date: 2007/02/24 17:27:43 $
 * 
 */public class PolicyException extends Exception {

  private static final long serialVersionUID = 1L;

  public PolicyException(String message) {
    super(message);
  }
  
}
