/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.policy;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;
import java.security.Permission;
import java.security.Principal;
import java.security.UnresolvedPermission;
import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.freshcookies.security.cert.JarHelper;

/**
 * <p>Utility class that dynamically looks up and returns Permission classes,
 * Principal classes and CodeSources corresponding to supplied strings. The factory
 * will attempt to resolve classes by consulting the parent classloader 
 * first, after which it will consult the URLs supplied to the class 
 * constructor {@link #SecurityTokenFactory(URL[])}. This allows the 
 * factory to load classes from arbirary JARs, although be warned that
 * this is a potential security hazard and should only be used in
 * carefully controlled situations.</p>
 * <p>If a SecurityManager is running, the security policy must grant 
 * this ProtectionDomain's CodeSource, and all preceding callers, 
 * the following permissions:</p>
 * <ul>
 *   <li><code>java.lang.RuntimePermission "accessClassInPackage.java.,javax."</code></li>
 *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-permission-packages</em>"</code></li>
 *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-principal-packages</em>"</code></li>
 *   <li><code>java.net.NetPermission "specifyStreamHandler"</code></li>
 * </ul>
 * <p>...where <em>custom-permission-packages</em> and
 * <em>custom-principal-packages</em> are the names of
 * custom {@link java.security.Permission} and {@link java.security.Principal}
 * classes loaded from external URLs.</p>
 * <p>In addition, if the <code>urls</code> parameter passed to the constructor is non-<code>null</code>
 * and has a length of 1 or more, the SecurityManager (if running) will require the
 * ProtectionDomain for this SecurityTokenFactory and all preceding callers to be 
 * granted these permissions:</p>
 * <ul>
 *   <li><code>java.lang.RuntimePermission "createClassLoader"</code></li>
 *   <li><code>java.io.FilePermission "<em>path-to-url.0</em>", "read"</code></li>
 *   <li><code>java.io.FilePermission "<em>path-to-url.1</em>", "read"</code></li>
 *   <li><code>java.io.FilePermission "<em>path-to-url.n</em>", "read"</code></li>
 * </ul>
 * <p>... for all file URLs whose names end in <code>.jar</code>. This is so 
 * that the {@link #getCodeSource(String)} method can successfully resolve and 
 * verify any jar file signatures.</p>
 * <p>None of the methods in this class perform their actions inside 
 * <code>doPrivileged</code> blocks, so all {@link SecurityException} errors
 * are propagated to callers.</code></p>
 * @author Andrew Jaquith
 * @version $Revision: 1.3 $ $Date: 2007/04/09 02:34:52 $
 */
public class SecurityTokenFactory {

	/** Constant that represents a single space character. */
	private static final String ONE_SPACE = " ";

	private static final int NOT_FOUND = -1;

	/** Constant that represents a double-quote character. */
	private static final String DOUBLE_QUOTE = "\"";

	/**
	 * Pattern that matches any valid Java type name, minus the
	 * <code>.java</code> or <code>.class</code> suffix.
	 */
	protected static final String REGEX_JAVA_TYPE = "((?:[a-zA-Z0-9$_]+\\.)*[A-Z$_][a-zA-Z0-9$_]*)";

	/**
	 * Pattern that matches permission string <em>e.g.</em>,
	 * <code>javax.security.auth.AuthPermission "setLoginConfiguration"</code>.
	 */
	protected static final Pattern PERMISSION_PATTERN = Pattern.compile("^"
			+ REGEX_JAVA_TYPE + "(?: ?\"(.*?)\")??(?: ?, ?\"(.*?)\")??$");

	private final ClassLoader classLoader;

	private final Map cachedCodeSources;

	private final Map cachedPermissions;
	
	private final Map cachedPrincipals;
	
	private final JarHelper jarHelper;
	
	private final Canonicalizer canonicalizer;

	/**
	 * <p>Constructs a new instance of this class with a supplied set of supplemental URLs for
	 * locating classes. The supplied URLs are used in addition to the
	 * parent ClassLoader that instantiated the SecurityTokenFactory.</p>
	 * <p>If the <code>urls</code> parameter passed to the constructor is non-<code>null</code>
	 * and has a length of 1 or more, the SecurityManager (if running) will require the
	 * ProtectionDomain for this SecurityTokenFactory and all preceding callers to be 
	 * granted these permissions:</p>
     * <ul>
     *   <li><code>java.lang.RuntimePermission "createClassLoader"</code></li>
     *   <li><code>java.io.FilePermission "<em>path-to-url.0</em>", "read"</code></li>
     *   <li><code>java.io.FilePermission "<em>path-to-url.1</em>", "read"</code></li>
     *   <li><code>java.io.FilePermission "<em>path-to-url.n</em>", "read"</code></li>
     * </ul>
	 * @param urls
	 *            the URLs used for resolving classes
	 * @throws SecurityException if a SecurityManager is running and any required
	 * permissions were not granted to this Protection Domain and all preceding callers
	 */
	public SecurityTokenFactory(URL[] urls) {
		if (urls != null && urls.length > 0) {
			this.classLoader = new URLClassLoader(urls);
		}
		else {
			this.classLoader = null;
		}
			
		this.cachedCodeSources = new HashMap();
		this.cachedPermissions = new HashMap();
		this.cachedPrincipals = new HashMap();
		this.canonicalizer = new Canonicalizer();
		this.jarHelper = new JarHelper();
	}

	/**
	 * <p>
	 * Returns the Principal that corresponds to a supplied String containing
	 * the name of the Principal class and the Principal's name, separated by
	 * spaces. This method will attempt to instantiate the class using
	 * reflection, passing the Principal name into the constructor. If the
	 * Principal class is not specified, a generic Principal class will be
	 * instantiated instead.
	 * </p>
	 * <p>
	 * Principal Strings look like this:
	 * </p>
	 * <ul>
	 * <li><code>com.ecyrd.jspwiki.auth.WikiPrincipal "Ernesto"</code></li>
	 * <li><code>javax.security.auth.kerberos.KerberosPrincipal "Ernesto@example.com"</code></li>
	 * <li><code>"Ernesto"</code></li>
	 * </ul>
	 * 
	 * @param s
	 *            the string specifying the Principal class and name
	 * @return the instantiated Principal
	 * @throws SecurityException when the SecurityManager, if running, denies 
	 * the protection domain of this class (and of all preceding callers):
	 * @throws SecurityException when the SecurityManager, if running, denies 
	 * the protection domain of this class (and of all preceding callers):
     * <ul>
     *   <li><code>java.lang.RuntimePermission "createClassLoader"</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.java.,javax."</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-principal-packages</em>"</code></li>
     * </ul>
	 */
	public Principal getPrincipal(String s) {
		
		// If found in cache, return it
		if (cachedPrincipals.containsKey(s)) {
			return (Principal) cachedPrincipals.get(s);
		}
		
		int space = s.indexOf(ONE_SPACE);
		int quote = s.indexOf(DOUBLE_QUOTE);
		Principal principal = null;

		// If no principal class supplied, prepend a generic principal class
		if (space == NOT_FOUND || quote < space) {
			s = GenericPrincipal.class.getName() + ONE_SPACE + s;
			space = s.indexOf(ONE_SPACE);
		}

		// Next, try to instantiate principal class with a 1-arg constructor
		String principalClass = s.substring(0, space).trim();
		String principalName = s.substring(principalClass.length()).trim();
		if (principalName.startsWith(DOUBLE_QUOTE)
				&& principalName.endsWith(DOUBLE_QUOTE)
				&& principalName.length() > 2) {
			principalName = principalName.substring(1,
					principalName.length() - 1);
		}
		try {
			Class clazz = findClass(principalClass);
			Constructor c = clazz.getConstructor(new Class[] { String.class });
			Object p = c.newInstance(new Object[] { principalName });
			principal = (Principal) p;
		} catch (Exception e) {
			principal = new UnresolvedPrincipal(principalClass, principalName,
					e);
		}
		
		// Stash principal and return it
		cachedPermissions.put(s, principal);
		return principal;
	}

	/**
	 * Returns the number of Principal objects cached by this factory since
	 * the last time {@link #reset()} was called.
	 * @return the number
	 */
	public int getPrincipalCount() {
		return cachedPrincipals.size();
	}
	
	/**
	 * <p>
	 * Returns the Permission that corresponds to a supplied String, without
	 * canonicalizing the Permission prior to returning it. 
   * See {@link #getPermission(String, boolean)}.
	 * @see #getPermission(String, boolean)
	 */
	public Permission getPermission(String permissionString)
		throws ClassNotFoundException {
		return getPermission(permissionString, false);
	}
	
	/**
	 * <p>
	 * Returns the Permission that corresponds to a supplied String. The String
	 * contains the full class name, target and actions, and is formatted
	 * exactly like a Java policy file grant statement. Permissions must have at
	 * least a target; actions are optional.
	 * </p>
	 * <p>
	 * Permission strings look like this:
	 * </p>
	 * <ul>
	 * <li><code>java.util.PropertyPermission "java.security.auth.login.config", "write"</code></li>
	 * <li><code>javax.security.auth.AuthPermission "setLoginConfiguration"</code></li>
	 * </ul>
	 * <p>
	 * Assuming the Permission string is well-formed, this method
	 * <em>always</em> returns a Permission, even if the Permission cannot be
	 * located by the classloader. If the Permission class cannot be loaded, the
	 * returned Permission will be of type
	 * {@link java.security.UnresolvedPermission}. Permission strings that
	 * contain just one argument, for example the class name but no target or
	 * actions, will be unresolved.
	 * </p>
	 * <p>If the parameter <code>canonicalize</code> is <code>true</code>, the Permission
	 * will be canonicalized before it is returned. Specifically, if the Permission is 
	 * of type {@link java.io.FilePermission}, its target will 
	 * be the canonical path. Likewise, {@link java.net.SocketPermission} local host names 
	 * 127.0.0.1 and "" will be converted to <code>localhost</code>.</p>
	 * 
	 * @param permissionString
	 *            the Permission string
	 * @param canonicalize
	 *            whether to canonicalize the Permission before returning it
	 * @return the instantiated Permission
	 * @throws ClassNotFoundException
	 *             if the Permission string is malformed
	 * @throws SecurityException when the SecurityManager, if running, denies 
	 * the protection domain of this class (and of all preceding callers):
     * <ul>
     *   <li><code>java.lang.RuntimePermission "createClassLoader"</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.java.,javax."</code></li>
     *   <li><code>java.lang.RuntimePermission "accessClassInPackage.<em>custom-permission-packages</em>"</code></li>
     * </ul>
	 */
	public Permission getPermission(String permissionString, boolean canonicalize)
			throws ClassNotFoundException {

		// If found in cache, return it
		if (cachedPermissions.containsKey(permissionString)) {
			return (Permission) cachedPermissions.get(permissionString);
		}
		
		String permissionClass = null;
		String target = null;
		String actions = null;

		// Extract the permission class name, target and actions
		Matcher m = PERMISSION_PATTERN.matcher(permissionString);
		if (m.find()) {
			permissionClass = m.group(1);
			if (m.group(3) != null) {
				target = m.group(2);
				actions = m.group(3);
			} else if (m.group(2) != null) {
				target = m.group(2);
			}
		} else {
			throw new ClassNotFoundException("Malformed permission: "
					+ permissionString);
		}

		// Try and instantiate the class... if exceptions, return an
		// UnresolvedPermission
		Permission perm = null;
		try {
			// Find the class we need
			Class clazz = findClass(permissionClass);

			// Prepare the constructor and arguments
			Class[] constructorArgClasses = new Class[0];
			String[] constructorArgs = new String[0];

			if (actions != null) {
				constructorArgs = new String[] { target, actions };
				constructorArgClasses = new Class[] { String.class,
						String.class };
			} else if (target != null) {
				constructorArgs = new String[] { target };
				constructorArgClasses = new Class[] { String.class };
			}
			Constructor c = clazz.getConstructor(constructorArgClasses);

			// Instantiate the class
			perm = (Permission) c.newInstance(constructorArgs);
		} catch (Exception e) {
			perm = new UnresolvedPermission(permissionClass, target, actions,
					null);
		}
		
		// Canonicalize the permission if asked
		if (canonicalize) {
			perm = canonicalizer.canonicalize(perm);
		}
		
		// Stash permission and return it
		cachedPermissions.put(permissionString, perm);
		return perm;
	}

	/**
	 * Returns the number of Permission objects cached by this factory since
	 * the last time {@link #reset()} was called.
	 * @return the number
	 */
	public int getPermissonCount() {
		return cachedPermissions.size();
	}
	
  
  /**
   * <p>
   * Returns the CodeSource that corresponds to a supplied String, without
   * canonicalizing the path prior to returning it. 
   * See {@link #getCodeSource(String, boolean, boolean)}.
   * @see #getCodeSource(String, boolean, boolean)
   */
  public CodeSource getCodeSource(String path) throws IOException, MalformedURLException {
    return getCodeSource(path, false);
  }
  
	/**
	 * <p>Returns the CodeSource corresponding on a specified file path. If the
	 * path ends in <code>.jar</code>, this method will attempt to load the
	 * Jar file and instantiate the CodeSource with its associated certificates,
	 * if possible. To improve performance, CodeSources are cached.</p>
   * <p>If the parameter <code>canonicalize</code> is <code>true</code>, the path
   * will be canonicalized before it is returned.</p>
	 * 
	 * @param path
	 *            the absolute path of the jar
   * @param canonicalize
   *            whether to canonicalize the Permission before returning it
	 * @return the CodeSource
	 * @throws IOException
	 *            if the file cannot be found in the filesystem
	 * @throws MalformedURLException
	 *            if the URL for the CodeSource is malformed
	 * @throws SecurityException when the SecurityManager, if running, denies 
	 * the protection domain of this class (and of all preceding callers):
     * <ul>
     *   <li><code>java.io.FilePermission "<em>path</em>", "read"</code></li>
     *   <li><code>java.net.NetPermission "specifyStreamHandler"</code></li>
     * </ul>
     *
	 */
	public CodeSource getCodeSource(String path, boolean canonicalize) throws IOException,
			MalformedURLException {
		// TODO: determine if this method is really needed...
		
		// Trim path to remove file: prefix
		if (path.startsWith("file:")) {
			path = path.substring(5);
		}

		// If found in cache, return it
		if (cachedCodeSources.containsKey(path)) {
			return (CodeSource) cachedCodeSources.get(path);
		}

		// Otherwise: create new code source
		File file = new File(path);
		if (!PolicyReader.secureExists(file)) {
			throw new IOException("Path " + path + " does not exist.");
		}

    // Canonicalize path if requested
    if (canonicalize) {
      path = file.getCanonicalPath();
    }
    
		// If path ends in .jar, try and load the Jar and its certificates
		CodeSource cs = null;
		if (path.endsWith(".jar")) {
			JarFile jar = new JarFile(new File(path));
			Set certSet = jarHelper.extractSigningCertificates(jar);
			Certificate[] certs = (Certificate[]) certSet
					.toArray(new Certificate[certSet.size()]);
			cs = new CodeSource(new URL("file:" + path), certs);
		} else {
			cs = new CodeSource(new URL("file:" + path), new Certificate[0]);
		}
    
		// Stash codesource and return it
		cachedCodeSources.put(path, cs);
		return cs;
	}
	
	/**
	 * Returns the number of CodeSource objects cached by this factory since
	 * the last time {@link #reset()} was called.
	 * @return the number
	 */
	public int getCodeSourceCount() {
		// TODO: determine if this method is really needed...
		return cachedCodeSources.size();
	}

	/**
	 * Looks up and returns a Class matching a supplied fully-qualified type
	 * name. The current ClassLoader is tried first; if not found, the classpath
	 * supplied in the URLs used to construct this ClassResolver will be tried
	 * next.
	 * 
	 * @param className
	 *            the name of the Class to find
	 * @return the resolved Class
	 * @throws ClassNotFoundException
	 *             if the class cannot be found
	 * @throws SecurityException when the SecurityManager, if running, denies 
	 * the protection domain of this class (and of all preceding callers)
	 * any of these permissions: <code>java.lang.RuntimePermission "getClassLoader"</code>,
	 * <code>java.lang.RuntimePermission "accessClassInPackage.<em>package-name-of-className"</em></code>.
	 */
	public Class findClass(String className) throws ClassNotFoundException {
		Class cls = null;

		// Try the parent classloader first
		try {
			cls = this.getClass().getClassLoader().loadClass(className);
			return cls;
		} catch (ClassNotFoundException e) {
			// The parent classloader couldn't find it!
		}

		// Try the URLs supplied in this resolver
		if (classLoader != null) {
			return classLoader.loadClass(className);
		}
		throw new ClassNotFoundException("Class not found: " + className);
	}
	
	/**
	 * Flushes the SecurityTokenFactory's caches and resets all counters.
	 */
	public synchronized void reset() {
		cachedCodeSources.clear();
		cachedPermissions.clear();
		cachedPrincipals.clear();
	}

}
