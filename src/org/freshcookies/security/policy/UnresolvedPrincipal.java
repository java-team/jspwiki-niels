/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.policy;

import java.security.Principal;

/**
 * Represents a Principal whose class cannot be resolved by the classloader.
 * 
 * @author Andrew Jaquith
 * @version $Revision: 1.2 $ $Date: 2007/02/24 17:27:43 $
 */
public final class UnresolvedPrincipal implements Principal {

  private final String name;

  private final String clazz;
  
  private final Exception exception;

  /**
   * Constructs a new UnresolvedPrincipal.
   * 
   * @param clazz the name of the unresolved class
   * @param name the name of the Principal
   */
  public UnresolvedPrincipal(String clazz, String name) {
	  this(clazz, name, null);
  }
  
  /**
   * Constructs a new UnresolvedPrincipal.
   * @param clazz the name of the unresolved class
   * @param name the name of the Principal
   * @param e the Exception indicating the reason why the Principal could not be resolved
   */
  public UnresolvedPrincipal(String clazz, String name, Exception e)
  {
	    this.clazz = clazz;
	    this.name = name;
	    this.exception = e;
  }

  /**
   * Returns the Exception used to instantiate this UnresolvedPrincipal.
   * @return the Exception, or <code>null</code> if not constructed with one.
   */
  public final Exception getException() {
	  return exception;
  }
  
  /**
   * Returns the class of the unresolved Principal as a String.
   * 
   * @return the String representation of the unresolved Principal class
   */
  public final String getPrincipalClass() {
    return clazz;
  }

  /**
   * Returns the name of the principal.
   * 
   * @see java.security.Principal#getName()
   */
  public final String getName() {
    return name;
  }

  /**
   * Returns a string representation of this Principal.
   * 
   * @see java.lang.Object#toString()
   */
  public final String toString() {
    return "(unresolved " + clazz + " " + name + ")";
  }

}
