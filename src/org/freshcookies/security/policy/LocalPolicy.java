/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.freshcookies.security.policy;

import java.io.File;
import java.io.IOException;
import java.security.AccessController;
import java.security.CodeSource;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Principal;
import java.security.PrivilegedAction;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * Security policy class that implements a subset of
 * {@link java.security.Policy} methods, using a standard Java security policy
 * file as input. This class is ideally suited for "local authorization" cases
 * where consultation of a global JVM policy is not necessary or desired, but
 * where polices are expressed in the standard policy file grammar. This class
 * differs from standard Policy implementations in that it does not extend the
 * Policy superclass, and it implements only two of its methods:
 * {@link #implies(ProtectionDomain, Permission)} and {@link #refresh()}.
 * Because LocalPolicy does not extend Policy, it cannot be installed as a
 * system-wide security policy (which is probably a good thing, because it is
 * not optimized as expertly as Sun's standard PolicyFile implementation).
 * </p>
 * <p>
 * The constructor of this class accepts a {@link java.io.File} from which the
 * policy is parsed and read using the {@link PolicyReader} class. The policy is
 * static and is derived <em>only</em> from the content of the file. If the
 * policy file changes in the file system, callers may reload it by calling
 * {@link #refresh()}.
 * </p>
 * <p>
 * This class is not thread-safe. It requires the same permissions to run 
 * as the underlying PolicyReader used to parse the policy file. 
 * (See {@link PolicyReader} for more details.
 * </p>
 * 
 * @author Andrew Jaquith
 * @version $Revision: 1.2 $ $Date: 2007/04/09 02:34:52 $
 * @see PolicyReader
 */
public class LocalPolicy {

	private boolean loaded;

	private final File policyFile;

	private final Set pds;
  
  private final String charset;
	
	/**
	 * Constructs a new instance of a LocalPolicy object, whose policy rules are
	 * parsed from a supplied File using the standard Java platform encoding. 
   * The policy file's contents and structure are
	 * expected to be identical to those used in the J2SE default policy file 
   * implementation.
	 * 
	 * @param file
	 *            the file supplying the security policy
	 */
	public LocalPolicy(File file) {
    this(file, (String)AccessController.doPrivileged( new PrivilegedAction() { 
      public Object run() { return System.getProperty("file.encoding"); } }));
	}

  /**
   * Constructs a new instance of a LocalPolicy object, whose policy rules are
   * parsed from a supplied File using the standard Java platform encoding. 
   * The policy file's contents and structure are
   * expected to be identical to those used in the J2SE default policy file 
   * implementation.
   * 
   * @param file
   *            the file supplying the security policy
   * @param encoding
   *            the charset name, such as <code>UTF-8</code> or <code>ISO-8859-1</code>
   */
  public LocalPolicy(File file, String encoding) {
    super();
    loaded = false;
    policyFile = file;
    pds = new HashSet();
    charset = encoding;
  }
  
	/**
	 * Returns <code>true</code> if the security policy grants a particular
	 * Permission to a ProtectionDomain whose code source, certificates and
   * classloader match the one supplied as a parameter. This method contains
   * the exact same method signature as {@link java.security.Policy#implies(ProtectionDomain, Permission)}
   * but differs in a key respect: Permissions for the supplied ProtectionDomain are
   * looked up based on its CodeSource, ClassLoader and Principals, rather
   * than looking for one that is logically equal to it.
	 * 
	 * @param domain
	 *            the protection domain
	 * @param permission
	 *            the permission to check for
	 * @return <code>true</code> if the permission was granted,
	 *         <code>false</code> if not
	 * @throws IllegalStateException if the policy needed refreshing but could
	 * not be refreshed
	 */
	public boolean implies(ProtectionDomain domain, Permission permission) {

		// If policy file not loaded, do it now
		if (!loaded) {
			try {
				refresh();
			}
			catch (PolicyException e) {
				throw new IllegalStateException(e.getMessage());
			}
		}

		// Iterate through the policy domains and find
		// one whose codesource, principals and classloader match
		for (Iterator it = pds.iterator(); it.hasNext();) {
			LocalProtectionDomain pd = (LocalProtectionDomain)it.next();
			boolean samePrincipals = samePrincipals(pd.getPrincipals(), domain.getPrincipals());
			boolean sameCodeSource = pd.getCodeSource().implies(domain.getCodeSource());
			boolean sameClassLoader = pd.getClassLoader().equals(domain.getClassLoader());
			if (samePrincipals && sameCodeSource && sameClassLoader) {
				
				// Ok! All we have to do now is see if our local policy domain grants the permission
				// (if not, keep going...)
        PermissionCollection permissions = pd.getPermissions();
				if (permissions.implies(permission)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Determines whether two Principal arrays contain the same items, which may appear in any order
	 * @param p1 the first array
	 * @param p2 the second array
	 * @return <code>true</code> if every item in the first array is <code>equal</code> to one other
	 * item in the second array; <code>false</code> otherwise
	 */
	protected boolean samePrincipals(Principal[] p1, Principal[] p2) {
		if (p1.length != p2.length) {
			return false;
		}
		
		// Set up a list of 'leftover' items that we pop from every time we find a match
		List leftovers = new ArrayList();
		for (int i = 0; i < p2.length; i++) {
			leftovers.add(p2[i]);
		}
		
		// Go through first array and remove the corresponding principal from leftover collection
		for (int i = 0; i < p1.length; i++) {
			Principal p = p1[i];
			for (Iterator it = leftovers.iterator(); it.hasNext();) {
				Principal leftover = (Principal)it.next();
				if (p.equals(leftover)) {
					it.remove();
					break;
				}
			}
		}
		// If nothing left in leftovers, everything must've matched
		return (leftovers.size() == 0);
	}
	
	/**
	 * <p>Reloads the policy from the filesystem. This method contains
   * the exact same method signature as {@link java.security.Policy#refresh()}
   * and is functionally equivalent. If the policy does not parse
	 * correctly, it will print any errors to standard out and throw an
	 * IllegalStateException. This method does not itself require security
	 * permissions to run, although the underlying PolicyReader used to parse
	 * the policy file does. (See {@link PolicyReader#read()} for
	 * more details.</p>
	 * @throws PolicyException if the policy could not be refreshed or parsed
	 */
	public void refresh() throws PolicyException {
		pds.clear();
		PolicyReader reader;
		try {
			reader = new PolicyReader(policyFile,charset);
			reader.read();
		} catch (IOException e) {
			String msg = "Problems refreshing policy from file " + policyFile
					+ ": " + e.getMessage();
			System.err.println(msg);
			throw new PolicyException(msg);
		}

		// If errors, print them out
		if (!reader.isValid()) {
			List messages = reader.getMessages();
			for (Iterator it = messages.iterator(); it.hasNext();) {
				String message = (String) it.next();
				System.err.println(message);
			}
			StringBuffer s = new StringBuffer();
			for (Iterator it = messages.iterator(); it.hasNext();) {
				s.append((String) it.next());
			}
			throw new PolicyException("Errors parsing policy from file "
					+ policyFile + ": " + s.toString());
		}

		// It parsed and validated ok, so now stash local copies of the protection domains
		ProtectionDomain[] parsedPds = reader.getProtectionDomains();
		for (int i = 0; i < parsedPds.length; i++) {
      ProtectionDomain pd = parsedPds[i];
      LocalProtectionDomain localPd = new LocalProtectionDomain(pd.getCodeSource(), pd.getPermissions(), pd.getClassLoader(), pd.getPrincipals());
			pds.add(localPd);
		}

		loaded = true;
	}
  
  /**
   * Lightweight re-implementation of {@link java.security.ProtectionDomain} that
   * does not allow subsequent modifications to Permissions. The default ProtectionDomain
   * is, unfortunately, unsuitable for use with LocalPolicy because default JVM permissions
   * are added "dynamically" subsequent to creation. This class exhibits the same
   * basic behaviors as ProtectionDomain: <code>null</code> values are allowed for the
   * LocalProtectionDomain's Principals, CodeSource and PermissionCollection. Also, if the
   * Principal array supplied is <code>null</code>, it will be converted to a zero-length
   * array.
   * @author Andrew Jaquith
   */
  public static class LocalProtectionDomain { 
    
    private final CodeSource codesource;
    private final ClassLoader classloader;
    private final PermissionCollection permissions;
    private final Principal[] principals;
    
    /**
     * Constructs a new LocalProtectionDomain.
     * @param codesource the code source; may be <code>null</code>
     * @param permissions the permission collection; may be <code>null</code>
     * @param principals the principals to whom the permissions are granted; may be <code>null</code>
     */
    public LocalProtectionDomain(CodeSource codesource, 
      PermissionCollection permissions, ClassLoader classloader, Principal[] principals) {
      this.codesource = codesource;
      this.permissions = permissions;
      if (permissions != null && !permissions.isReadOnly()) {
        permissions.setReadOnly();
      }
      this.classloader = classloader;
      this.principals = (principals == null) ? new Principal[0] : principals;
    }
    
    /**
     * Returns the ClassLoader used to construct the LocalProtectionDomain,
     * which may be <code>null</code>.
     * @return the class loader
     */
    public ClassLoader getClassLoader() {
      return classloader;
    }
    
    /**
     * Returns the Principal array used to construct the LocalProtectionDomain,
     * if supplied, or a zero-length array if not supplied.
     * @return the principals
     */
    public Principal[] getPrincipals() {
      return principals;
    }
    
    /**
     * Returns the PermissionCollection used to construct the LocalProtectionDomain,
     * which may be <code>null</code>.
     * @return the permissions
     */
    public PermissionCollection getPermissions() {
      return permissions;
    }
    
    /**
     * Returns the CodeSource used to construct the LocalProtectionDomain,
     * which may be <code>null</code>.
     * @return the principals
     */
    public CodeSource getCodeSource() {
      return codesource;
    }
  }

}
